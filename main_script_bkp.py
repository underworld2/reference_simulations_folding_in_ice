#!/usr/bin/python
# coding: utf-8

import matplotlib.pyplot as pyplot
import numpy as np
import math
import os

import time as ti

import underworld as uw

from underworld import function as fn
import underworld.visualisation as vis

import pdb

outputPath = os.path.join(os.path.abspath("."),"output/")
#if uw.rank() == 0:
if not os.path.exists(outputPath):
    os.makedirs(outputPath)
#uw.barrier()


viz_opts = {
                "figsize"     : (2000,400),
                "edgecolour"   :  "black",
                "quality"      :  3,          # antialiasing
                "align"        : "bottom",     # colour bar alignment
                #"size"        : (0.83,0.01), # colour bar size
                #"position"    : 0.,          # colour bar position
                #"boundingBox"  : ((MIN_X, MAX_X), (MIN_Y, MAX_Y), (minZ, maxZ)),
                "axis"         : True,
                "scale"        : True,
           }



'''
The variables below serve as defaults, but can
be scripted, too.
insert the revised values best below the next comment.
'''
total_width = 100000.
maxY        = 2500.
is_at_divide = False
vs = 5. # m/a. this number is only relevant if we are NOT at the divide, as this is the horizontal influx. vs is the velocity at the surface.
n = 3     # A will be set automatically below
tempMax = -1.
tempMin = -30.
fold_amplitude = 500.
fold_width = 5000.
annual_snowfall = 0.2
maxViscosityIce     = 7.37e13
minViscosityIce     = 1e11

number_of_deformation_lines = 11
delta_timestep = 50.                    # in years, used in the main loop
update_figures_after_n_timesteps = 5    # after how many timesteps do we need new figures?
'''
place redefined values below
'''
number_of_deformation_lines = 11
resY = 32
resX = 128
minViscosityIce = 1e+11
maxViscosityIce = 2e+13
annual_snowfall = 0.0
fold_width = 5000.0
fold_amplitude = 1000.0
tempMin = -30.0
tempMax = 0.0
n = 3
vs = 0.0
is_at_divide = False
maxY = 2500.0
total_width = 100000.0

'''
end of section with redefined values
'''

'''
additional settings, which are currently fixed for a set of simulations
'''
velocity_vector_arrows_in_y = 8
velocity_vector_arrows_in_x = 125
velocity_vector_net_min_width = min(total_width / velocity_vector_arrows_in_x, maxY / velocity_vector_arrows_in_y)

initial_width = total_width
minX        = -total_width / 2.
maxX        = total_width / 2.

T_independent_flow = True
'''A and n for n = 3'''
if n == 3:
    A = 1.2e-6 #--> this is in MPa^-3
elif n == 4:
    '''A and n for n = 4'''
    A = 3.3e-5 #--> this is in MPa^4
else:
    print("T-independent flow has not been implemented yet")
    exit(0)

'''A and n for T-dependent flow -- also change the bool variable above!!!'''
# A = 2.75e19 * fn.math.exp(- QFn / ((temperatureField + 273.) * 8.3144) ) #--> this would be in MPa^4
# n = 4

number_of_deformation_points = 20000
distance_between_deformation_lines = maxY / (number_of_deformation_lines + 1)

vs /= (365*86400.) # need vs in m/s

# annual snowfall (ice equivalent) in m
snowfall_rate = annual_snowfall / (365 * 86400)
snowmass_per_second = snowfall_rate * total_width
meshV = snowmass_per_second * total_width / maxY
# print (meshV)

# resX = 50
# resY = 25

# below we calculate the scale for the velocity arrows. scale depends on velocity
if n == 3:
    prefac = 0.5 / 0.4 * snowmass_per_second
elif n == 4:
    prefac = 0.5 / 0.4 * snowmass_per_second / maxY + vs
else:
    print ("Check the scale factor - it hasn't been implemented for the n you gave!")
    exit(0)

scale_v = prefac / maxY + vs
if scale_v != 0.:
    scale_factor_for_vector_arrows = velocity_vector_net_min_width / scale_v
else:
    scale_factor_for_vector_arrows = 2.5e10 # this is nothing but an estimate derived from the expected scale of the vertical velocity

### inclination of the ice sheet surface is between 1-2 m / 5000 m (central Greenland) and 20 m / 5000 m (towards the edges)
### this function is needed in order to calculate the background strain rate
###
### we have to ways to determine the surface inclination:
### a) directly insert an angle
### b) calculate from the flux we need, as the surface slope determines the shear stress
###
### it isn't used if we have velocity boundary conditions that reflect the snow fall and the horizontal flux.
surf_inclination = np.arctan(12. / 5000.)  #this is an angle of 0.12 deg, resulting in a surface velocity of 10 m / a if n=3


# ~ elementType="Q2/dPc1"    # This is enough for a test but not to use the code in anger
elementType="Q1/dQ1"        # 'uw.system.AdvectionDiffusion' implementation is only stable for a phiField discretised with a Q1 mesh

mesh = uw.mesh.FeMesh_Cartesian( elementType    = (elementType),
                 elementRes     = ( resX, resY),
                 minCoord       = ( minX, 0.),
                 maxCoord       = ( maxX, maxY),
                 periodic       = [False, False]  )

# save the mesh
mesh.save( outputPath + "mesh.h5")

velocityField   = uw.mesh.MeshVariable( mesh=mesh, nodeDofCount=mesh.dim )
pressureField   = uw.mesh.MeshVariable( mesh=mesh.subMesh, nodeDofCount=1 )

temperatureField    = mesh.add_variable( nodeDofCount=1 )
temperatureDotField = mesh.add_variable( nodeDofCount=1 )

strainRateField        = mesh.add_variable( nodeDofCount=1 )

pressureField.data[:] = 0.
temperatureDotField.data[:] = 0.

# ### Boundary conditions
#
# Pure shear with moving  walls — all boundaries are zero traction with

iWalls = mesh.specialSets["MinI_VertexSet"] + mesh.specialSets["MaxI_VertexSet"]
jWalls = mesh.specialSets["MinJ_VertexSet"] + mesh.specialSets["MaxJ_VertexSet"]
base   = mesh.specialSets["MinJ_VertexSet"]
top    = mesh.specialSets["MaxJ_VertexSet"]

allWalls = iWalls + jWalls

velocityField.data[:] = [0.,0.]
velocityField.data[top,1] = -snowfall_rate

### old version
# velocityField.data[mesh.specialSets["MinI_VertexSet"], 0] = -meshV / 2.
# velocityField.data[mesh.specialSets["MaxI_VertexSet"], 0] = meshV / 2.

'''
if we are at a divide, we have similar flux to both sides of the section. no influx, except for snowfall.

if we are NOT at a divide, we have influx from the left. velocity increases throughout the section, in order to
get rid of the extra mass due to snowfall.
'''
if is_at_divide:
    if T_independent_flow:
        if n == 3:
            prefac = 0.5 * snowmass_per_second / 0.4 # 0.5 because flow goes to 2 directions: left + right
            print ("Prefactor == " + str(prefac))
            for i,z in enumerate (mesh.data[mesh.specialSets["MinI_VertexSet"],1]):
                velocityField.data[mesh.specialSets["MinI_VertexSet"][i], 0] = -0.5 * prefac * (maxY**4 - (maxY - z)**4) / maxY**5 # function of z
            for i,z in enumerate (mesh.data[mesh.specialSets["MaxI_VertexSet"],1]):
                velocityField.data[mesh.specialSets["MaxI_VertexSet"][i], 0] = 0.5 * prefac * (maxY**4 - (maxY - z)**4) / maxY**5 # function of z
        elif n == 4:
            prefac = 0.5 * snowmass_per_second / 0.426 # 0.5 because flow goes to 2 directions: left + right
            print ("Prefactor == " + str(prefac))
            for i,z in enumerate (mesh.data[mesh.specialSets["MinI_VertexSet"],1]):
                velocityField.data[mesh.specialSets["MinI_VertexSet"][i], 0] = -0.5 * prefac * (maxY**5 - (maxY - z)**5) / maxY**6 # function of z
            for i,z in enumerate (mesh.data[mesh.specialSets["MaxI_VertexSet"],1]):
                velocityField.data[mesh.specialSets["MaxI_VertexSet"][i], 0] = 0.5 * prefac * (maxY**5 - (maxY - z)**5) / maxY**6 # function of z
        else:
            print ("n has non-supported value")
            exit(0)
    else:
        print ("The flow equation for T-dependent flow has still to be implemented!!!")
        exit (0)
else:
    if T_independent_flow:
        if n == 3:
            prefac = 0.5 * snowmass_per_second / 0.4 # 0.5 because flow goes to 2 directions: left + right

            # left boundary
            for i,z in enumerate (mesh.data[mesh.specialSets["MinI_VertexSet"],1]):
                velocityField.data[mesh.specialSets["MinI_VertexSet"][i], 0] = vs * (1. - (maxY - z)**4 / maxY**4) # function of z
            # right boundary
            for i,z in enumerate (mesh.data[mesh.specialSets["MaxI_VertexSet"],1]):
                velocityField.data[mesh.specialSets["MaxI_VertexSet"][i], 0] = vs * (1. - (maxY - z)**4 / maxY**4) + prefac * (maxY**4 - (maxY - z)**4) / maxY**5 # function of z
        elif n == 4:
            prefac = 0.5 * snowmass_per_second / 0.426 # 0.5 because flow goes to 2 directions: left + right

            # left boundary
            for i,z in enumerate (mesh.data[mesh.specialSets["MinI_VertexSet"],1]):
                velocityField.data[mesh.specialSets["MinI_VertexSet"][i], 0] = vs * (1. - (maxY - z)**5 / maxY**5) # function of z
            # right boundary
            for i,z in enumerate (mesh.data[mesh.specialSets["MaxI_VertexSet"],1]):
                velocityField.data[mesh.specialSets["MaxI_VertexSet"][i], 0] = vs * (1. - (maxY - z)**5 / maxY**5) + prefac * (maxY**5 - (maxY - z)**5) / maxY**6 # function of z
        else:
            print ("n has non-supported value")
            exit(0)
    else:
        print ("The flow equation for T-dependent flow has still to be implemented!!!")
        exit (0)

# print(velocityField.data[top])
# print(velocityField.data[mesh.specialSets["MinI_VertexSet"]])
# print(velocityField.data[mesh.specialSets["MaxI_VertexSet"]])

velocityBCs = uw.conditions.DirichletCondition( variable        = velocityField,
                                                indexSetsPerDof = (base + iWalls, base + top) )

tempBCs = uw.conditions.DirichletCondition( variable        = temperatureField,
                                            indexSetsPerDof = (allWalls) )

# ### Setup the material swarm and passive tracers
#
# The material swarm is used for tracking deformation and history dependence of the rheology
#
# Passive swarms can track all sorts of things but lack all the machinery for integration and re-population

part_per_cell = 50
swarm  = uw.swarm.Swarm( mesh=mesh, particleEscape=True )
swarmLayout = uw.swarm.layouts.PerCellSpaceFillerLayout( swarm=swarm, particlesPerCell=part_per_cell )
swarm.populate_using_layout( layout=swarmLayout )

coord = fn.input()

surfaceSwarm = uw.swarm.Swarm( mesh=mesh, particleEscape=True )
deformationSwarm = uw.swarm.Swarm ( mesh=mesh, particleEscape=True )
initialFoldSwarm = uw.swarm.Swarm ( mesh=mesh, particleEscape=True )

# create pop control object
pop_control1 = uw.swarm.PopulationControl(swarm, aggressive=True, particlesPerCell=part_per_cell )
pop_control2 = uw.swarm.PopulationControl(surfaceSwarm)
pop_control3 = uw.swarm.PopulationControl(deformationSwarm)
pop_control4 = uw.swarm.PopulationControl(initialFoldSwarm)

# ### Create a particle advection system
#
# Note that we need to set up one advector systems for each particle swarm (our global swarm and a separate one if we add passive tracers).

advector    = uw.systems.SwarmAdvector( swarm=swarm,            velocityField=velocityField, order=2 )
advector2   = uw.systems.SwarmAdvector( swarm=surfaceSwarm,     velocityField=velocityField, order=2 )
advector3   = uw.systems.SwarmAdvector( swarm=deformationSwarm, velocityField=velocityField, order=2 )
advector4   = uw.systems.SwarmAdvector( swarm=initialFoldSwarm, velocityField=velocityField, order=2 )

# Tracking different materials

materialVariable = swarm.add_variable( dataType="int", count=1 )

# These ones are for monitoring of the shear bands
orientation = swarm.add_variable( dataType="double", count=1)

# passive markers at the surface are inserted whenever 100 m of new snow hav been created in the main loop

deformationPoints = np.array(np.meshgrid(np.linspace(minX , maxX , int( number_of_deformation_points)), np.linspace(0., maxY, number_of_deformation_lines ))).T.reshape(-1,2)
deformationSwarm.add_particles_with_coordinates( deformationPoints )

surfacePoints = np.zeros((int( number_of_deformation_points),2))
surfacePoints[:,0] = np.linspace(minX , maxX , int(number_of_deformation_points))
surfacePoints[:,1] = maxY
surfaceSwarm.add_particles_with_coordinates( surfacePoints )

director = swarm.add_variable( dataType="double", count=2 )
c_angle = swarm.add_variable( dataType="double", count=1 )

particleTemperature = swarm.add_variable( dataType="double", count=1 )
particleDensity = swarm.add_variable( dataType="double", count=1 )

particleBackgroundStrain = swarm.add_variable( dataType="double", count=1 )
particleBackgroundStrain.data[:] = 0.0

figT = vis.Figure(**viz_opts, title="Temperature")
#figT.append(vis.objects.Points(swarm, materialVariable, pointSize=1.0, fn_mask=materialVariable, colourBar=False))
figT.append(vis.objects.Points(swarm, particleTemperature, pointSize=1.0, colourBar=True))
figT.save("Temp-distribution-png")

# ### Initialise swarm variables

# orientation of the director - describes the plane of preferred slip direction
# we set this for _all_ materials, so it's there if air is transformed to ice
angle = np.random.uniform(0, 2.0 * math.pi, swarm.particleGlobalCount)
angle = np.pi / 2.
# direcor pointing upwards in the standard setting
director.data[:,0] = np.cos(angle)
director.data[:,1] = np.sin(angle)

c_angle.data[:,0] = np.arctan(director.data[:,1] / director.data[:,0])


# This is a work-variable for visualisation

orientation.data[:] = 0.0

particleTemperature.data[:] = 0.0
particleDensity.data[:] = 0.0

# plastic strain - weaken a region at the base close to the boundary (a weak seed but through cohesion softening)

def gaussian(xx, centre, width):
    return ( np.exp( -(xx - centre)**2 / width ))

def boundary(xx, minX, maxX, width, power):
    zz = (xx - minX) / (maxX - minX)
    return (np.tanh(zz*width) + np.tanh((1.0-zz)*width) - math.tanh(width))**power

# weight = boundary(swarm.particleCoordinates.data[:,1], 10, 4)

#if uw.nProcs() == 1:    # Serial
xx = np.arange(-20, 20, 0.01)
yy = boundary(xx, minX, maxX, 10, 2)
pyplot.scatter(xx,yy)

# ### Material distribution in the domain.
#

# Initialise the 'materialVariable' data to represent different materials.
materialA   = 0 # accommodation layer, a.k.a. Sticky Air
materialV   = 1 # ice, isotropic
materialU   = 2 # Under layer = water
materialVD  = 3 # ice, anisotropic
materialWI  = 4 # ice, warm
materialHA  = 5 # hard air, at the top of the model

# The particle coordinates will be the input to the function evaluate (see final line in this cell).
# We get proxy for this now using the input() function.

coord = fn.input()

# Setup the conditions list for the following conditional function. Where the
# z coordinate (coordinate[1]) is less than the perturbation, set to lightIndex.

conditions = [  #( coord[1] > 0.9 * maxY , materialA ),
                #( coord[1] < 0.3 * maxY , materialU ),
                (True , materialV) ]

# The actual function evaluation. Here the conditional function is evaluated at the location
# of each swarm particle. The results are then written to the materialVariable swarm variable.

materialVariable.data[:] = fn.branching.conditional( conditions ).evaluate(swarm)

strainRateTensor            = fn.tensor.symmetric( velocityField.fn_gradient )
strainRate_2ndInvariantFn   = fn.tensor.second_invariant( strainRateTensor )

temperatureField.data[:] = -30.
materialVariable.data[:] = materialV

# ### Viscosity (7.6.19)
minViscosityIceFn   = fn.misc.constant(minViscosityIce)
maxViscosityIceFn   = fn.misc.constant(maxViscosityIce)

# we simply use the density of ice at -30 deg, which makes up the largest part of the ice sheet
den = 18.02 / (19.30447 - 7.988471e-4 * 243 + 7.563261e-6 * 243**2) * 1e3
coord = fn.input()
surf_slope = np.sin(surf_inclination)
vertical_strainrate = annual_snowfall / (maxY * 365*86400)

### if we don't need an artifical background strain, because the snowfall and and the horizontal extension are included in the boundary conditions (the velocity field).
### This should be the standard case
backgroundStrainRateFnNIs3 = fn.misc.constant(0.)
backgroundStrainRateFnNIs4 = fn.misc.constant(0.)
### if the velocity field is 0 at its boundaries
#backgroundStrainRateFnNIs3 = 1.2e-6 * (surf_slope * 9.81 * den * (maxY - fn.input()[1]) * 1e-6)**3 + (vertical_strainrate) * (fn.input()[1] / maxY)
#backgroundStrainRateFnNIs4 = 3.3e-5 * (surf_slope * 9.81 * den * (maxY - fn.input()[1]) * 1e-6)**4 + (vertical_strainrate) * (fn.input()[1] / maxY)

if T_independent_flow:
    if n == 3:
        # viscosityfunction: convert to Pa.s by multiplying with 1e6
        # + 1e-18: avoid a 0-strain rate
        viscosityFnIceBase    = (.5 * A**(-1.0/n) * (strainRate_2ndInvariantFn + 1e-18 + backgroundStrainRateFnNIs3)**(1.0/n - 1.0)) * 1e6
        viscosityFnIce        = fn.misc.max(fn.misc.min(viscosityFnIceBase, maxViscosityIceFn), minViscosityIce)
    elif n ==4:
        #viscosityfunction: convert to Pa.s by multiplying with 1e6
        # + 1e-18: avoid a 0-strain rate
        viscosityFnIceBase = (.5 * A**(-1.0/n) * (strainRate_2ndInvariantFn + 1e-18 + backgroundStrainRateFnNIs4)**(1.0/n - 1.0)) * 1e6
        viscosityFnIceBase = (A * strainRate_2ndInvariantFn + 1e-18)**(1.0/n - 1.0)
        viscosityFnIce       = fn.misc.max(fn.misc.min(viscosityFnIceBase, maxViscosityIceFn), minViscosityIce)

else: # T-dependent flow

    if n == 4:
        Q = 120000.
        QFn = fn.misc.constant(Q)

        #viscosityfunction: convert to Pa.s by multiplying with 1e6
        # + 1e-18: avoid a 0-strain rate
        viscosityFnIceBase = (.5 * A**(-1.0/n) * (strainRate_2ndInvariantFn + 1e-18)**(1.0/n - 1.0)) * 1e6
        #viscosityFnIceBase = (A * strainRate_2ndInvariant + 1e-18)**(1.0/n - 1.0)
        viscosityFnIce       = fn.misc.max(fn.misc.min(viscosityFnIceBase, maxViscosityIceFn), minViscosityIce)
    else:
        print ("Check your n / T-depndance settings!")
        exit(0)

'''---------------------------------------------'''

''' newtonian viscosity - just uncomment to apply '''
#viscosityIce   = 2e13 / time_scale
#viscosityIceFn = fn.misc.constant(minViscosityIce)

''' end of newtonian section  '''



print ( np.max( viscosityFnIce.evaluate(swarm)[:] ) )

''' common viscosity section '''
viscosityFnIce2 = 0.98 * viscosityFnIce # isotropic ice

# map
viscosityMap        = { materialV:viscosityFnIce }
viscosity2Map       = { materialV:viscosityFnIce2 }

firstViscosityFn       = fn.branching.map( fn_key = materialVariable, mapping = viscosityMap )
secondViscosityFn      = fn.branching.map( fn_key = materialVariable,  mapping = viscosity2Map )

viscosityField       = mesh.add_variable( nodeDofCount=1 )
devStressFn         = 2.0 * firstViscosityFn * strainRateTensor

densityField           = mesh.add_variable( nodeDofCount=1 )

# ### Buoyancy forces
#
# In this example, no buoyancy forces are considered. However, to establish an appropriate pressure gradient in the material, it would normally be useful to map density from material properties and create a buoyancy force.

#densityMap = { materialHA: 0.0, materialA: 0.0, materialV:1.0, materialU:1.1, materialVD:1.0 }
#densityFn = fn.branching.map( fn_key=materialVariable, mapping=densityMap )

'''
\rho_{ice} = \frac{M}{V_0} * \frac{1e6}{1e3}
'''
densityFnIce = 18.02 / (19.30447 - 7.988471e-4 * (273.0 + particleTemperature) + 7.563261e-6 * (273.0 + particleTemperature)**2) * 1e3
densityMap = { materialHA: 0.0, materialA: 0.0, materialV:densityFnIce, materialU:1.1, materialVD:1.0, materialWI:densityFnIce }
densityFn = fn.branching.map( fn_key=materialVariable, mapping=densityMap )

tConductivityFnIce = 9.828 * fn.math.exp(-0.0057 * (273.0 + particleTemperature))
specificHeatFnIce = 146.3 + 7.253 * (273.0 + particleTemperature)

tDiffusivityFnIce = tConductivityFnIce / (specificHeatFnIce * densityFnIce)
tDiffusivityMap = { materialHA: 0.0, materialA: 0.0, materialV:tDiffusivityFnIce, materialU:0.0, materialVD:0.0, materialWI:tDiffusivityFnIce }
tDiffusivityFn = fn.branching.map( fn_key=materialVariable, mapping=tDiffusivityMap )

# And the final buoyancy force function.
#z_hat = ( 0.0, 1.0 ) # standard case

inclinationAngle = 0.
z_hat = (-math.sin(inclinationAngle/ 90. * math.pi/2.) , math.cos(inclinationAngle / 90. * math.pi/2.))

print ()
print ("**************************")
print (z_hat)
print ("**************************")
print ()

buoyancyFn = -densityFn * z_hat
particleDensity.data[:] = densityFn.evaluate(swarm)

# System setup
# -----
#
# Setup a Stokes equation system and connect a solver up to it.
#
# In this example, no buoyancy forces are considered. However, to establish an appropriate pressure gradient in the material, it would normally be useful to map density from material properties and create a buoyancy force.

stokes = uw.systems.Stokes( velocityField   = velocityField,
                            pressureField   = pressureField,
                            voronoi_swarm   = swarm,
                            conditions      = velocityBCs,
                            fn_viscosity    = firstViscosityFn,
                            _fn_viscosity2  = secondViscosityFn,
                            _fn_director    = director,
                            fn_bodyforce    = buoyancyFn )

advDiff = uw.systems.AdvectionDiffusion(    phiField        = temperatureField,
                                            phiDotField     = temperatureDotField,
                                            velocityField   = velocityField,
                                            fn_diffusivity  = tDiffusivityFn,
                                            conditions      = tempBCs )

solver = uw.systems.Solver( stokes )

## Initial solve (drop the non-linearity the very first solve only)

# "mumps" is a good alternative for "lu" but
# not every petsc installation has mumps !
# It also works fine in parallel

# use "lu" direct solve and large penalty (if running in serial)
#if(uw.nProcs()==1):
solver.set_inner_method("lu")
#solver.set_inner_method("mg")
#solver.set_inner_method("mumps")
solver.set_penalty(1.0e10) # higher penalty = larger stability
#solver.options.scr.ksp_rtol = 1.0e-3

# test it out
#nl_tol = 1.e-4
#solver.solve( nonLinearIterate=True, nonLinearTolerance=nl_tol)
solver.solve( nonLinearIterate=True)
solver.print_stats()

surfaceArea = uw.utils.Integral(fn=1.0,mesh=mesh, integrationType='surface', surfaceIndexSet=top)
surfacePressureIntegral = uw.utils.Integral(fn=pressureField, mesh=mesh, integrationType='surface', surfaceIndexSet=top)

(area,) = surfaceArea.evaluate()
(p0,) = surfacePressureIntegral.evaluate()

pressureField.data[:] -= p0 / area

directorVector = uw.mesh.MeshVariable( mesh, 2 )
directorProjector = uw.utils.MeshVariable_Projection( directorVector, director, type=1 )
directorProjector.solve()

# The stress is only guaranteed to be accurate when integrated across an element. Fluctuations
# within an element can be significant. Mapping to the mesh can help

meshDevStress = uw.mesh.MeshVariable( mesh, 1 )

projectorStress = uw.utils.MeshVariable_Projection( meshDevStress, fn.tensor.second_invariant(devStressFn), type=0 )
projectorStress.solve()

# ## Main simulation loop
# -----

# create an array which will be used to advect/update the mesh
mesh_vels = meshV*np.copy(mesh.data[:,0])/maxX

# also create projection object
projectorStress = uw.utils.MeshVariable_Projection( meshDevStress, fn.tensor.second_invariant(devStressFn), type=0 )

def density_from_temperature(T):

    # T in Kelvin!
    if T < 0:
        raise ValueError('T needs to be in Kelvin!')

    M = 18.01528 # molar mass in g/mol
    V_0 = 1.930447e1 - 7.988471e-4 * T + 7.563261e-6 * T**2 # molar volume in cm^3

    rho = M / V_0 * 1e3

    return rho

def add_gauss_distributed_value_to_field_variable(fieldvar, sigma):

    mean = 0.
    length = mesh.nodesLocal

    minimum = np.min(fieldvar.data[:,0])
    maximum = np.max(fieldvar.data[:,0])

    delta_min_max = np.abs(minimum - maximum)

    randomField = np.random.normal(0., sigma, length)
    # normalize to field value
    # randomField *= fieldvar.data[:,0] / delta_min_max

    fieldvar.data[:,0] += randomField

    fieldvar.data[:,0] = np.clip(fieldvar.data[:,0], minimum, maximum)

def setup_linear_temperature_profile(sigma = 0.):
    """
    Parameters: the gaussian distributions standard deviation.

    tempMin / tempMax are global variables, set somewhere before the function is called.

    Currently this is just a linear semi-dummy function
    Nicer would be something like Radok, Jenssen & Budd's (1970), Equation 12
    """

    # ~ (tempMax, tempMin) = (T, 0.) if T > 0. else (0., T)

    deltaTemp = tempMin - tempMax

    # first set the temperature for the mesh variable
    for index, coord in enumerate(mesh.data):
        temperatureField.data[index] = tempMin + deltaTemp*(coord[1] / maxY - 1.0)
        temperatureField.data[index] = max(tempMin, min(tempMax, temperatureField.data[index]))

    if sigma != 0.:
        add_gauss_distributed_value_to_field_variable(temperatureField, sigma)

    # now set the temperature in the particle variable
    particleTemperature.data[:] = temperatureField.evaluate(swarm)

def bumpfunc(x, xmin, xmax, amplitude):

    """
    Calculates the shape of a sinusoidal bump,
    function of x, returns y-value

    can be added to some base line (const y) by the calling function

    Like so:
           _
    ______/ \_____
    ______________
    """

    k = 2. * np.pi / abs(xmax - xmin) # scaling x to the cosine function

    return (0. if x < xmin or x > xmax else (np.cos(x * k) + 1.) * amplitude / 2. )

def setup_T_bump(y0 = 800., T0 = tempMin, Tbed = tempMax, amplitude = fold_amplitude, width = fold_width):
    """
    A simple approximation for T that increases from depth z=0 with T0 to z=z(bed) with Tbed (-3°C?) is:

    z>C: T(z) = T0

    z<C: T(z)=    T0 + (Tbed - T0) * ( 1. - y / deltay )**2)

    Question is were C is (about 2/3 down towards the bed?).

    For the initial bump you can do: C(x) = C0 - ∆C exp(-4x^2/a^2)
    This is Bercovici's fold, which has an amplitude of ∆C and a width of 2a. Within the fold, the thermal profile is simply stretched, while above the temperature is T0 (say -33°).

    !!! The function as implemented assumes that the bed is at z = 0 !!!
    """

    global temperatureField
    global mesh

    temperatureField.data[:] = T0

    # the bump
    xmidpoint, halfwavelength = 0., width/2.
    xmin = xmidpoint - halfwavelength
    xmax = xmin + width

    for index, coord in enumerate (mesh.data):

        # 'Tbed-core' situation
        ybase = bumpfunc(coord[0], xmin, xmax, amplitude)

        if coord[1] <= ybase + y0:
            temperatureField.data[index] = T0 + (Tbed - T0) * (1. - (coord[1] - ybase) / y0 )**2

        if coord[1] <= ybase:
            temperatureField.data[index] = Tbed

    particleTemperature.data[:] = temperatureField.evaluate(swarm)

def setup_sinusoidal_plume_instability(temp = 0., width = 1000., A = 1000., basal_height = 0.):
    """
    This function will set a sinusoidal area to a given temperature.
    Area is located on top of a basal layer with the same temperature.

    Parameters: T, width (half wavelength) and height (amplitude) of the instability, height of basal layer.

    Like so:
           _
    ______/ \_____
    ______________
    """

    temperatureField.data[:] = tempMin

    minx = -width / 2.0
    maxx = width / 2.0
    miny = basal_height
    maxy = basal_height + A
    k = np.pi / (2. * maxx) # scaling x to the cosine function

    for index, coord in enumerate(mesh.data):
        if coord[1] < basal_height:
            temperatureField.data[index] = temp
        elif coord[1] < maxy and coord[0] > minx and coord[0] < maxx:
            x = coord[0] * k
            if coord[1] < np.cos(x) * A + basal_height:
                temperatureField.data[index] = temp

    # now set the temperature in the particle variable
    particleTemperature.data[:] = temperatureField.evaluate(swarm)

def setup_triangular_plume_instability(temp = 0., width = 2500., A = 500., basal_height = 500.):
    """
    This function will set a sinusoidal area to a given temperature.
    Area is located on top of a basal layer with the same temperature.

    Parameters: T, width (half wavelength) and height (amplitude) of the instability, height of basal layer.

    Like so:
           _
    ______/ \_____
    ______________
    """

    minx = -width / 2.0
    maxx = width / 2.0
    miny = basal_height
    maxy = basal_height + A

    for index, coord in enumerate(mesh.data):
        if coord[1] < basal_height:
            temperatureField.data[index] = temp
        elif coord[1] < maxy and coord[0] > minx and coord[0] < 0.:
            x = coord[0] + width/2.
            m = A / maxx
            if coord[1] < basal_height + m*x:
                temperatureField.data[index] = temp
        elif coord[1] < maxy and coord[0] > 0. and coord[0] < maxx:
            x = coord[0]
            m = - A / maxx
            if coord[1] < basal_height + m*x + A:
                temperatureField.data[index] = temp

    # now set the temperature in the particle variable
    particleTemperature.data[:] = temperatureField.evaluate(swarm)

def setup_density_profile_from_temperature():

    global densityFn
    global swarm
    global particleDensity

    particleDensity.data[:] = densityFn.evaluate(swarm)

def c_axis_rotation(dt):

    iceIndices = np.array(np.where(materialVariable.data == materialVD)[0])

    velGrad = velocityField.fn_gradient.evaluate(  swarm ).reshape(swarm.particleGlobalCount , 2, 2)
    velGrad = velGrad[iceIndices]
    velGradT = velGrad.swapaxes(-1,1)

    # rate of deformation and rate of rotation
    D = 0.5 * (velGrad + velGradT)
    W = 0.5 * (velGrad - velGradT)

    director.data[iceIndices] = director.data[iceIndices] + dt * ( np.einsum("ijk,ik->ij", W, director.data[iceIndices]) - np.einsum("ijk,ik->ij", D, director.data[iceIndices]) + np.einsum("ij,ij->i",director.data[iceIndices], np.einsum("ijk,ik->ij",D,director.data[iceIndices]))[:,None] * director.data[iceIndices])

    #finally normalize the c-axes
    director.data[iceIndices] = director.data[iceIndices] / np.absolute(np.linalg.norm(director.data[iceIndices], axis=1).reshape(len(iceIndices),1))

def update_extension():
    # get timestep and advect particles
    dt = advector.get_max_dt()

    advector.integrate( dt )
    advector2.integrate( dt, update_owners=False )
    advector3.integrate( dt, update_owners=False )

    # Stretch mesh to match boundary conditions
    # Note that this also calls the update_particle_owners for the
    # attached swarms
    with mesh.deform_mesh( isRegular=True ):
        mesh.data[:,0] += mesh_vels[:]*dt

    newtime = time + dt
    # recalc mesh extension
    newminX = minX - meshV * newtime
    newmaxX = maxX + meshV * newtime

    # particle population control
    pop_control1.repopulate()
    # pop_control2.repopulate()
    # pop_control3.repopulate()

    # update orientation of c-axes
    c_axis_rotation(dt)

    return newtime, step+1

def update_infinite_flow( t = 100.):
    """
    In order for infinite flow to work you need to
    a) use the same medication as MC Escher,
    b)    1) generate 'inclination' --> z_hat definition
        2) activate wrapping by setting 'periodic = [True, False]' during the mesh creation
    """

    global meshV, maxX, minX, maxY, total_width, snowfall_rate

    t = t * 365.*86400.
    tout = 0.

    while t > 0.:
        # Retrieve the maximum possible timestep for the advection system.
        #dt = advector.get_max_dt()
        dt = advDiff.get_max_dt()

        # increase stability of the solution with smaller timesteps
        dt = 0.5 * dt

        if dt > t:
            dt = t
            t = 0.
        else:
            t = t - dt

        advDiff.integrate(dt)

        tout = tout+dt
        #print (tout)

        advector2.integrate( dt )
        advector3.integrate( dt )
        advector4.integrate( dt )

        # Advect using this timestep size.
        advector.integrate(dt)

        particleTemperature.data[:] = temperatureField.evaluate(swarm)

        ''' the code in this block will extend the mesh horizontally
        meshV = snowfall_rate * total_width / maxY
        mesh_vels = meshV * np.copy(mesh.data[:,0]) / maxX
        print(snowfall_rate, maxX, minX, total_width, meshV)

        with mesh.deform_mesh( isRegular=True ):
            mesh.data[:,0] += mesh_vels[:]*dt

        maxX = mesh.maxCoord[0]
        minX = mesh.minCoord[0]
        total_width = maxX - minX
        '''

        # particle population control
        # not sure, if this is really necessary. it is _not_ in the standard advecton code, but only if there is a mesh-deformation involved. my hope would be that this command will not allow underresolved cells
        pop_control1.repopulate()
        # pop_control2.repopulate()
        # pop_control3.repopulate()

        # update orientation of c-axes
        c_axis_rotation(dt)

    return time+tout, step+1



def set_buoyancy_inclination(inclinationAngle):

    """
    note: this DOES NOT WORK!!!

    -- I don't know why, though --

    all the important variables are set to 'global', yet it seems that this has no effect on the global functions

    set in degree deviation from the positive x-axis clockwise
    """

    global z_hat
    global buoyancyFn
    global densityFn

    z_hat = (-math.sin(inclinationAngle), math.cos(inclinationAngle))
    buoyancyFn = -densityFn * z_hat

def update_advection_diffusion():

    dt = min([advector.get_max_dt(), advector2.get_max_dt(), advDiff.get_max_dt()])

    advector2.integrate( dt, update_owners=False )
    advector.integrate(dt)
    advDiff.integrate(dt)

    # particle population control
    #pop_control.repopulate()

    # update orientation of c-axes
    c_axis_rotation(dt)

    particleTemperature.data[:] = temperatureField.evaluate(swarm)

    return time+dt, step+1


def define_caxes_warped_by_folding(saveFile=False):
    '''
    this function defines an indexed 2D array, where single positions store the angles 
    of c-axes at the given location.
    
    basis for the calculation is the assumption that:
        
        a) the fold amplitude decreases toward the surface (where it is 0) and is at a max at the base of the model
        b) this permits the calculation of the amplitude at every height
        c) the width of the fold is calculated by its area: because the displaced mass has to be the same at any height within the ice sheet, the fold width has to increase if the amplitude decreases. Fold width is inf if amplitude is 0 at the surface
        d) basis for the fold calculation is a cosine curve, which is approximated by the first to members of the corresponding taylor series (which is a bit easier to calculate)
    '''

    print ("In c-axis setup")
    
    amplitude = fold_amplitude
    width = fold_width
    surface_height = maxY
    
    size, H0, L0, ys = total_width, amplitude, width, surface_height

    # slope of the amplitude function (decreases to 0 at surface)
    c = -H0 / (ys - H0)
    H0p = H0 - c*H0
    
    x = np.linspace(-size,size,2*int(size))
    
    angle_arr = np.ones((x.shape[0], int(ys)), dtype=np.double) * np.pi/2.

    y = lambda x, H, yb, L : (1 - 2*(x/L)**2 + 4*(x/L)**4) * 4*H - 3*H + yb if x > -L/2. and x < L/2. else 0.
    yarr = np.vectorize(y)

    # angle of director = arctan of slope + np.pi/2
    y_angle = lambda x, L : np.arctan(-16*H*x/(L**2) + 64*H*x**3/(L**4)) + np.pi/2. if x > -L/2 and x < L/2 else np.pi/2.
    y_anglearr = np.vectorize(y_angle)

    for ymid in np.arange(0., ys, 0.5):

        H = H0p + c*ymid
        yb = ymid - H

        if H == 0.:
            L = inf
        else:
            L = np.divide(H0 * L0, H)
        
        angle_arr[x.astype(int)+int(size)-1, yarr(x, H, yb, L).astype(int)] = y_anglearr(x, L)
    
    pos = fn.input().evaluate(swarm)
    
    director.data[:,0] = np.cos(angle_arr[pos[:,0].astype(int)+int(size),pos[:,1].astype(int)])
    director.data[:,1] = np.sin(angle_arr[pos[:,0].astype(int)+int(size),pos[:,1].astype(int)])
    
    # save data to a file, open it with fct 'load_caxes_from_file'
    if saveFile:
        np.save('c_axes_angles.npy', angle_arr)
    
    print ("Max / Min angle of the c-axes: " + str(np.max(angle_arr)) + ", " + str(np.min(angle_arr)) )

    # we already defined a swarm (initialFoldSwarm), which has now to be filled with particles
    initialFoldPoints = np.zeros((int(number_of_deformation_points),2))
    initialFoldPoints[:,0] = np.linspace(minX , maxX , int(number_of_deformation_points))

    # redefine lambda function in order to return y, not 0, outside the fold
    y = lambda x, H, yb, L : (1 - 2*(x/L)**2 + 4*(x/L)**4) * 4*H - 3*H + yb if x > -L/2. and x < L/2. else yb
    yarr = np.vectorize(y)

    for y in np.arange(0, maxY, distance_between_deformation_lines):

        H = H0p + c*y
        yb = y - H

        if H == 0.:
            L = inf
        else:
            L = np.divide(H0 * L0, H)

        initialFoldPoints[:,1] = yarr(initialFoldPoints[:,0], H, yb, L)
        initialFoldSwarm.add_particles_with_coordinates( initialFoldPoints )
        
    calc_caxes_angles()

def main():

    # Stepping. Initialise time and timestep.
    global time, step
    time = 0.
    step = 0
    nsteps = 201

    setup_T_bump(y0 = 800., T0 = tempMin, Tbed = tempMax, amplitude = fold_amplitude, width = fold_width)
    setup_density_profile_from_temperature()

    z = ti.time()
    define_caxes_warped_by_folding(amplitude = fold_amplitude, width = fold_width, surface_height = maxY)
    print ("Zeit: " + str(ti.time() - z) )

    newsurf = 0.

    while step < nsteps:
        # Obtain V,P and remove null-space / drift in pressure
        solver.solve( nonLinearIterate=True )
        (area,) = surfaceArea.evaluate()
        (p0,) = surfacePressureIntegral.evaluate()
        pressureField.data[:] -= p0 / area

        newsurf += annual_snowfall * delta_timestep

        if newsurf >= distance_between_deformation_lines:
            # 1 ts=100 a, 5 ts = 500 a, 500 a = 100 m snowfall --> mark the new surface
            surfacePoints = np.zeros((int(number_of_deformation_points),2))
            surfacePoints[:,0] = np.linspace(minX , maxX , int(number_of_deformation_points))
            surfacePoints[:,1] = maxY
            surfaceSwarm.add_particles_with_coordinates( surfacePoints )

            newsurf = 0.

        if (step % update_figures_after_n_timesteps == 0):

            '''
            need to save the raw data on the cluster, because X doesn't work in singularity container..
            '''
            # save the mesh variables
            velocityField.save( outputPath + "meshV-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5" )
            pressureField.save( outputPath + "meshP-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5" )
            temperatureField.save( outputPath + "meshT-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5" )
            directorProjector.solve()
            directorVector.save( outputPath + "directorVector-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5" )
            meshDevStress.save( outputPath + "devStress-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5" )
            strainRateField.data[:] = strainRate_2ndInvariantFn.evaluate(mesh)
            strainRateField.save( outputPath + "strainRate-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5" )

            # evaluate the viscosity function
            viscosityField.data[:] = viscosityFnIce.evaluate(mesh)
            viscosityField.save( outputPath + "viscosity-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5" )
            densityField.data[:] = densityFn.evaluate(mesh)
            densityField.save( outputPath + "density-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5" )

            # save the swarm
            #swarm.save(outputPath + "Swarm-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5" )
            surfaceSwarm.save(outputPath + "surfaceSwarm-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5" )
            deformationSwarm.save(outputPath + "deformationSwarm-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5" )
            initialFoldSwarm.save(outputPath + "initialFoldSwarm-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5" )

            # save the swarm variables
            #director.save(outputPath + "particleDirector-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5")
            #c_angle.save(outputPath + "particle.C_angle-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5")
            #particleTemperature.save(outputPath + "particleT-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5")
            #particleDensity.save(outputPath + "particleDensity-" + str(step).zfill(4) + " - " + str(time / (365.0 * 86400)) + ".h5")

        #if uw.rank()==0:
        #print('step = {0:6d}; time = {1:.3e};'.format(step,time))

        """
        advection
        (do NOT combine with advection diffusion, see below)
        """
        # finished timestep, update all, depending on the type of deformation
        #time, step = update_extension()

        time, step = update_infinite_flow( t = delta_timestep )

        """
        advection-diffusion
        (this calls advection internally)
        """
        #time, step = update_advection_diffusion()
        setup_density_profile_from_temperature()

if __name__== "__main__":
    main()
