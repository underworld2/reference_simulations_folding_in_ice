# Documentation for the reference simulations with formulas

> Note: some formulas, etc are in the xmind file in this same directory.

## Physics
### Calculation of the maximum (newtonian) viscosity

Above this viscosity, a constant Newtonian viscosity is applied.

Here I assume that this is determined by the (in)famous 'crossover stress', which has been derived by Pettite (2011?) using an anisotropic n=3 flow law.

The crossover stress ($\tau_c$) after Pettit is 0.018 *MPa*.

Derivation of the equivalent viscosity $\eta_{eff}$:

(1) The general equation:
$$\eta_{eff} = \left[ \frac 1 2 \right] A^{-1/n} \dot \epsilon ^{(1-n)/n}$$
depending on the version of the power law, the 1/2 may be already integrated into the prefactor.
 
(2) Replace $\dot \epsilon$ by the appropriate power law:
$$\eta_{eff,c} = A^{-1/n} (A\cdot \tau_c^n) ^{(1-n)/n} $$
$$\eta_{eff,c} = A^{-1/n} A^{(1-n)/n} \cdot \tau_c^{1-n}$$
$$\eta_{eff, c} = \frac {\tau_c^{1-n}} {A}$$

### Backgroundstrain 
Without any ongoing strain / flow of the ice mass, the viscosity is simply to large for folding to occur.

We assume therefore a surface slopw of 0.1333 degree (== 0.00232704749168403962 rad, used in the funcs).

The reason for this particular number: 0.1333 deg will cause a surface velocity of about 5 m/a using the Kuiper version of Glens flow law - a realistic value.

## Code

### Set up orientation of c-axes after folding

(1) the function is called 'define_caxes_warped_by_folding'. It will calculate a map for the entire area, where each element contains the value of the director angle.

(2) this map (2D-array) will be saved and can be reopened by 'load_caxes_from_file', saving *a lot* of computation time.

The idea: 

(1) we use a cosine curve in order to describe the fold. this guarantees that - at coordinates -pi and +pi we will have a slope of 0, and also at the center of the system. The full amplitude is 2 (-1 till +1), so we have to add 1 and multiply with just the half of the fold amplitude.

	```py
	# I believe there was an error in the following function for very small Ls, 
    # therefore the need for an 'if' 
    def y(x, H, yb, L):
        if x > -L / 2. and x < L / 2:
            d = (( np.cos ( 2. * x / L * np.pi ) + 1. ) * H/2. ) + yb
            if d > 0:
                return (d)
            else:
                return (0.)
        else:
            return(0.)
	```
(2) After calculating the y positions for a lot of x-positions, we can calculate the slope + 90° for all the known positions now, and right them at the correct position into the table:

	```py
	def y_angle(x, L, H):
    return (np.arctan( np.pi * H * np.sin( -2. * np.pi * -x / L ) / L ) + np.pi/2. if x > -L and x < L else np.pi/2.) 
    y_anglearr = np.vectorize(y_angle)
	```
	
	This is mainly the derivative of above function.
	
(3) In addition we have to calculate the change of the fold geometry from bottom to top. This is just a vertical compression + a horizontal extension, area conservative. In a way, this is the most complicated part of the code to debug.


