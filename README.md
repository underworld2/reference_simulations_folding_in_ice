# reference_simulations_folding_in_ice

Topic: continental ice. Reference-simulations to be published on the DRT conference and in the cryosphere. Prove that differential loading is real, using a variety of different flow laws, geometries and temperature distributions.

