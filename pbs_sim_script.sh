#!/bin/bash

# Redirect output stream to this file.
#PBS -o pbs_out.dat

# Redirect error stream to this file.
#PBS -e pbs_err.dat

# Join error and log files
#PBS -j oe

# Send status information to this email address.
#PBS -M till.sachau@uni-tuebingen.de

# Send an e-mail when the job is done.
##PBS -m e

#PBS -q esd2
#PBS -l nodes=1:ppn=1:esd2
#PBS -l walltime=72:00:00
#PBS -l pmem=10gb

# Change to current working directory (directory where qsub was executed)
# within PBS job (workaround for SGE option "-cwd")
DIR=$PBS_O_WORKDIR
DIR=$(echo $DIR | sed s+[/][^/]*[/][^/]*+/beegfs/work+)
cd $DIR
echo $DIR

module load devel/singularity/3.2

# For example an additional script file to be executed in the current
# working directory. In such a case assure that script.sh has
# execution rights (chmod +x script.sh).

# singularity run --bind $DIR uw2.simg sim.py 1 > job_output.dat 2>&1
singularity exec --bind $DIR/../../../:/resources /beegfs/work/epssa01/foldmodel/uw2-v3.4.simg python script.py > job_output.dat 2>&1
