#!/usr/bin/env python
# coding: utf-8
# +
# # About
#
# * Here we are going to model a fold with the properties of the leysinger fold
# * this script is based on the old 'Experiment B' script where the model was scaled (ISMIP-HOM)
#

# # Basic python imports and model settings

import underworld.visualisation as vis

from underworld import function as fn
import underworld as uw

import matplotlib.pyplot as pyplot
import numpy as np
from scipy.spatial import distance

import math
import os

import time as ti

import pickle

# +
## basic parameters

MODEL_DATA = {}

'''with air layer
MODEL_DATA['MIN_Y'] = 0.0
MODEL_DATA['MAX_Y'] = 2750.0
MODEL_DATA['MAX_X'] = 11000.
MODEL_DATA['MIN_X'] = -MODEL_DATA['MAX_X']
MODEL_DATA['RES_X'] = 88
MODEL_DATA['RES_Y'] = 11
'''

'''without air layer
'''
MODEL_DATA['MIN_Y'] = 0.0
MODEL_DATA['MAX_Y'] = 2500.0
MODEL_DATA['MAX_X'] = 10000.
MODEL_DATA['MIN_X'] = -MODEL_DATA['MAX_X']
MODEL_DATA['RES_X'] = 80
MODEL_DATA['RES_Y'] = 10

ice_height = 2500.

total_width = MODEL_DATA['MAX_X'] - MODEL_DATA['MIN_X']

R = 0.008314  # kJ / (T*mol)

switch_flowlaw_temperature = 263.
crossover_stress = 0.018 # MPa

Q_high_T = 139.  # kJ/mol, activation energy
Q_low_T = 60.

A_high_T = 1.73e21
A_low_T = 3.61e5

n = 3.

MODEL_DATA['ELEMENT_TYPE'] = "Q1/dQ0"
MODEL_DATA['PERIODIC_X'] = False
MODEL_DATA['PERIODIC_Y'] = False
MODEL_DATA['PARTICLES_PER_CELL'] = 200

# save basic model data to file
# save a dictionary into pickle file
# generate output path
if not os.getcwd().rsplit("/")[-1] == "output":
    outputPath = os.path.join(os.path.abspath("."),"output/")
    if not os.path.exists ( outputPath ):
        os.makedirs ( outputPath )

    os.chdir(outputPath)
    
MODEL_DATA_FILE = outputPath + "model_data.p"
pickle.dump( MODEL_DATA, open(MODEL_DATA_FILE , "wb" ) )

viz_opts = {
                "figsize"     : (2000,400),
                "edgecolour"   :  "black",
                "quality"      :  3,          # antialiasing
                "align"        : "bottom",     # colour bar alignment
                #"size"        : (0.83,0.01), # colour bar size
                #"position"    : 0.,          # colour bar position
                #"boundingBox"  : ((MIN_X, MAX_X), (MIN_Y, MAX_Y), (minZ, maxZ)),
                "axis"         : True,
                "scale"        : True,
           }

number_of_deformation_lines = 15

number_of_deformation_points = 50000
distance_between_deformation_lines = MODEL_DATA['MAX_Y'] / (number_of_deformation_lines + 1)

# +
# # Fold and temperature

tempMin = -20
tempMax = 0
fold_amplitude = 500
fold_width = 5000

# +
# # Mesh + mesh variables

mesh = uw.mesh.FeMesh_Cartesian( elementType = ( MODEL_DATA['ELEMENT_TYPE'] ) ,
                                 elementRes  = ( MODEL_DATA['RES_X'], MODEL_DATA['RES_Y'] ),
                                 minCoord    = ( MODEL_DATA['MIN_X'], MODEL_DATA['MIN_Y'] ),
                                 maxCoord    = ( MODEL_DATA['MAX_X'], MODEL_DATA['MAX_Y'] ),
                                 periodic    = ( MODEL_DATA['PERIODIC_X'], MODEL_DATA['PERIODIC_Y'] )
                               )

submesh = mesh.subMesh

velocityField = uw.mesh.MeshVariable(mesh=mesh, nodeDofCount=mesh.dim)
pressureField = uw.mesh.MeshVariable(mesh=mesh.subMesh, nodeDofCount=1)
strainRateField = mesh.add_variable(nodeDofCount=1)

pressureField.data[:] = 0.
velocityField.data[:] = [0., 0.]
strainRateField.data[:] = 0.



# +
# # Initialise the 'materialVariable' data to represent different materials.
materialA = 0  # accommodation layer, a.k.a. Sticky Air
materialV = 1  # ice, isotropic
materialR = 2  # rock

# # Swarm
swarm = uw.swarm.Swarm(mesh=mesh, particleEscape=True)
swarmLayout = uw.swarm.layouts.PerCellSpaceFillerLayout( swarm=swarm, particlesPerCell=MODEL_DATA['PARTICLES_PER_CELL'])
swarm.populate_using_layout(layout=swarmLayout)

surfaceSwarm = uw.swarm.Swarm(mesh=mesh, particleEscape=True)
deformationSwarm = uw.swarm.Swarm(mesh=mesh, particleEscape=True)
initialFoldSwarm = uw.swarm.Swarm(mesh=mesh, particleEscape=True)

# create pop control object
pop_control1 = uw.swarm.PopulationControl(swarm, aggressive=True, particlesPerCell=MODEL_DATA['PARTICLES_PER_CELL'])
pop_control2 = uw.swarm.PopulationControl(surfaceSwarm)
pop_control3 = uw.swarm.PopulationControl(deformationSwarm)
pop_control4 = uw.swarm.PopulationControl(initialFoldSwarm)

# ### Create a particle advection system
#
# Note that we need to set up one advector systems for each particle swarm (our global swarm and a separate one if we add passive tracers).
advector1 = uw.systems.SwarmAdvector(swarm=swarm,
                                     velocityField=velocityField,
                                     order=2)
advector2 = uw.systems.SwarmAdvector(swarm=surfaceSwarm,
                                     velocityField=velocityField,
                                     order=2)
advector3 = uw.systems.SwarmAdvector(swarm=deformationSwarm,
                                     velocityField=velocityField,
                                     order=2)
advector4 = uw.systems.SwarmAdvector(swarm=initialFoldSwarm,
                                     velocityField=velocityField,
                                     order=2)

# Tracking different materials

materialVariable = swarm.add_variable(dataType="int", count=1)

# passive markers at the surface are inserted whenever 100 m of new snow hav been created in the main loop
deformationPoints = np.array(
    np.meshgrid(np.linspace(MODEL_DATA['MIN_X'], MODEL_DATA['MAX_X'], int(number_of_deformation_points)),
                np.linspace(0., MODEL_DATA['MAX_Y'],
                            number_of_deformation_lines))).T.reshape(-1, 2)
deformationSwarm.add_particles_with_coordinates(deformationPoints)

surfacePoints = np.zeros((int(number_of_deformation_points), 2))
surfacePoints[:, 0] = np.linspace(0., MODEL_DATA['MAX_X'], int(number_of_deformation_points))
surfacePoints[:, 1] = MODEL_DATA['MAX_Y']
surfaceSwarm.add_particles_with_coordinates(surfacePoints)

particleDensity = swarm.add_variable(dataType="float", count=1)
particleDensity.data[:] = 0.0

coord = fn.input()

#z_bed_function = maxY - average_bedthickness + amplitude * fn.math.sin(omega * coord[0])

conditions = [   (coord[1] > ice_height,  materialA),
    #(coord[1] < z_bed_function, materialR),
    (True, materialV)
]

materialVariable.data[:] = fn.branching.conditional(conditions).evaluate(swarm)

# +
director = swarm.add_variable(dataType="double", count=2)
director.data[:, 0] = 0.
director.data[:, 1] = 1.

#director.data[:,0] = np.random.random(director.data.shape[0]) - 0.5
#director.data[:,1] = np.random.random(director.data.shape[0]) - 0.5

cAngle = swarm.add_variable(dataType="double", count=1)
particleVelocity = swarm.add_variable(dataType="double", count=2)

particleTemperature = swarm.add_variable(dataType="double", count=1)
particleTemperature.data[:] = 0.

particleStrainrate = swarm.add_variable ( dataType="double", count=1 )
particleShearstress = swarm.add_variable ( dataType="double", count=1 )

def calc_caxes_angles():

    global director
    global cAngle

    director.data[np.where(director.data[:, 1] < 0.),
                  0] = -director.data[np.where(director.data[:, 1] < 0.), 0]
    director.data[np.where(director.data[:, 1] < 0.),
                  1] = -director.data[np.where(director.data[:, 1] < 0.), 1]
    cAngle.data[:, 0] = np.arctan2(director.data[:, 1], director.data[:, 0])


def c_axis_rotation(dt):

    iceIndices = np.array(np.where(materialVariable.data == materialV)[0])

    velGrad = velocityField.fn_gradient.evaluate(swarm).reshape(
        swarm.particleGlobalCount, 2, 2)
    velGrad = velGrad[iceIndices]
    velGradT = velGrad.swapaxes(-1, 1)

    # rate of deformation and rate of rotation
    D = 0.5 * (velGrad + velGradT)
    W = 0.5 * (velGrad - velGradT)

    director.data[iceIndices] = director.data[iceIndices] + dt * (
        np.einsum("ijk,ik->ij", W, director.data[iceIndices]) -
        np.einsum("ijk,ik->ij", D, director.data[iceIndices]) + np.einsum(
            "ij,ij->i", director.data[iceIndices],
            np.einsum("ijk,ik->ij", D, director.data[iceIndices]))[:, None] *
        director.data[iceIndices])

    # finally normalize the c-axes
    director.data[iceIndices] = director.data[iceIndices] / np.absolute(
        np.linalg.norm(director.data[iceIndices], axis=1).reshape(
            len(iceIndices), 1))

calc_caxes_angles()

# # Functions
densityFnA = fn.misc.constant( 0. )
densityFnR = fn.misc.constant( 2700. )
densityFnV = (18.02 / (19.30447 - 7.988471e-4 * (particleTemperature+273.) + 7.563261e-6 * ((particleTemperature+273.)**2) )) * 1000.

densityMap = {
    materialA: densityFnA,
    materialR: densityFnR,
    materialV: densityFnV,
}

densityFn = fn.branching.map(fn_key=materialVariable, mapping=densityMap)

particleViscosity = swarm.add_variable( dataType = "double", count=1 )

strainRateTensor = fn.tensor.symmetric(velocityField.fn_gradient)
strainRate_2ndInvariantFn = fn.tensor.second_invariant(strainRateTensor)

# backgroundstrein for 0.1333 degree slope (== 0.00232704749168403962 rad, used in the funcs below)
# reason: this is about 5 m/a for Glens flow law - a value to be expercted

if n ==3:
    yearly_surface_vel_low_T = A_low_T * fn.math.exp(-Q_low_T / (R*263)) * (920. * 9.81 * fn.math.sin(0.00174533) / 1e6)**3 * ice_height**4 * 365 * 86400
    yearly_surface_vel_high_T = A_high_T * fn.math.exp(-Q_high_T / (R*263)) * (920. * 9.81 * fn.math.sin(0.00174533) / 1e6)**3 * ice_height**4 * 365 * 86400

    bgStrainrate_low_T = A_low_T * fn.math.exp(-Q_low_T / (R*(particleTemperature + 273.))) * ((ice_height - coord[1]) * densityFn * 9.81 * fn.math.sin(0.00232704749168403962) / 1e6)**3
    bgStrainrate_high_T = A_high_T * fn.math.exp(-Q_high_T / (R*(particleTemperature + 273.))) * ((ice_height - coord[1]) * densityFn * 9.81 * fn.math.sin(0.00232704749168403962) / 1e6)**3
elif n== 4:
    yearly_surface_vel_low_T = 1/5. * A_low_T * fn.math.exp(-Q_low_T / (R*263)) * (920. * 9.81 * fn.math.sin(0.00174533) / 1e6)**4 * ice_height**5 * 365 * 86400
    yearly_surface_vel_high_T = 1/5. * A_high_T * fn.math.exp(-Q_high_T / (R*263)) * (920. * 9.81 * fn.math.sin(0.00174533) / 1e6)**4 * ice_height**5 * 365 * 86400

    bgStrainrate_low_T = A_low_T * fn.math.exp(-Q_low_T / (R*(particleTemperature + 273.))) * ((ice_height - coord[1]) * densityFn * 9.81 * fn.math.sin(0.00232704749168403962) / 1e6)**4
    bgStrainrate_high_T = A_high_T * fn.math.exp(-Q_high_T / (R*(particleTemperature + 273.))) * ((ice_height - coord[1]) * densityFn * 9.81 * fn.math.sin(0.00232704749168403962) / 1e6)**4
else:
    print ('somthing wrong with your n-value')
    exit(0)

# MPa s
viscosityFnAir = fn.misc.constant(1e1)
viscosityFnRock = fn.misc.constant(1e17)

minViscosityIceFn = fn.misc.constant(1e5)  # .. arbitrary
maxViscosityIceFn_low_T =  fn.math.pow(crossover_stress, 1.-n) / (A_low_T * fn.math.exp(-Q_low_T / (R * (particleTemperature + 273.))))
maxViscosityIceFn_high_T =  fn.math.pow(crossover_stress, 1.-n) / (A_high_T * fn.math.exp(-Q_high_T / (R * (particleTemperature + 273.))))

V_ice_low_T_raw = (A_low_T * fn.math.exp(-Q_low_T / (R * (particleTemperature + 273.))) )**(-1. / n) * ((strainRate_2ndInvariantFn + 1e-18 + bgStrainrate_low_T)**( (1. - n) / (n)))
V_ice_low_T = fn.misc.max(fn.misc.min(V_ice_low_T_raw, maxViscosityIceFn_low_T), minViscosityIceFn)

V_ice_high_T_raw = (A_high_T * fn.math.exp( -Q_high_T / (R * (particleTemperature + 273.))))**(-1. / n) * ((strainRate_2ndInvariantFn + 1e-18 + bgStrainrate_high_T)**( (1. - n) / float(n)))
V_ice_high_T = fn.misc.max(fn.misc.min(V_ice_high_T_raw, maxViscosityIceFn_high_T), minViscosityIceFn)

V_ice = [
    (particleTemperature <= (switch_flowlaw_temperature - 273), V_ice_low_T),
    (True, V_ice_high_T),
]

viscosityFnIce= fn.branching.conditional(V_ice) 
viscosityFnIce2 = viscosityFnIce * 0.99

viscosityMap = {
    materialA: viscosityFnAir,
    materialV: viscosityFnIce,
    materialR: viscosityFnRock,
}

viscosityMap2 = {
    materialA: viscosityFnAir,
    materialV: viscosityFnIce2,
    materialR: viscosityFnRock,
}

viscosityFn = fn.branching.map(fn_key=materialVariable, mapping=viscosityMap) * 1e6 # convert to Pascal
viscosityFn2 = fn.branching.map(fn_key=materialVariable, mapping=viscosityMap2) * 1e6

particleViscosity.data[:] = viscosityFn.evaluate(swarm)

logViscosityFn = fn.math.log10(viscosityFnIce * 1e6)

devStressFn = 2.0 * viscosityFn * strainRateTensor
shearStressFn = strainRate_2ndInvariantFn * viscosityFn * 2.0


# surf_inclination = 0.5 * np.pi / 180. # 0.1 = Experiment D, 0.5 = Experiment B
surf_inclination = 0.
z_hat = (math.sin(surf_inclination), -math.cos(surf_inclination))

buoyancyFn = densityFn * z_hat * 9.81


# +
def bumpfunc(x, xmin, xmax, amplitude):
    """
    Calculates the shape of a sinusoidal bump,
    function of x, returns y-value

    can be added to some base line (const y) by the calling function

    Like so:
           _
    ______/ \_____
    ______________
    """

    k = 2. * np.pi / abs(xmax - xmin)  # scaling x to the cosine function

    return (0. if x < xmin or x > xmax else
            (np.cos(x * k) + 1.) * amplitude / 2.)


def setup_T_bump_squared(y0=800.,
                 T0=tempMin,
                 Tbed=tempMax,
                 amplitude=fold_amplitude,
                 width=fold_width):
    """
    A simple approximation for T that increases from depth z=0 with T0 to z=z(bed) with Tbed (-3°C?) is:

    z>C: T(z) = T0

    z<C: T(z)=    T0 + (Tbed - T0) * ( 1. - y / deltay )**2)

    Question is were C is (about 2/3 down towards the bed?).

    For the initial bump you can do: C(x) = C0 - ∆C exp(-4x^2/a^2)
    This is Bercovici's fold, which has an amplitude of ∆C and a width of 2a. Within the fold, the thermal profile is simply stretched, while above the temperature is T0 (say -33°).

    !!! The function as implemented assumes that the bed is at z = 0 !!!

    """
    global particleTemperature
    global swarm

    particleTemperature.data[:] = T0

    # the bump
    xmidpoint, halfwavelength = 0., width / 2.
    xmin = xmidpoint - halfwavelength
    xmax = xmin + width

    for index, coord in enumerate(swarm.data):

        # 'Tbed-core' situation
        ybase = bumpfunc(coord[0], xmin, xmax, amplitude)

        if coord[1] <= ybase + y0:
            particleTemperature.data[index] = T0 + (Tbed - T0) * (
                1. - (coord[1] - ybase) / y0)**2

        if coord[1] <= ybase:
            particleTemperature.data[index] = Tbed

def setup_T_bump_linear(y0=800.,
                 T0=tempMin,
                 Tbed=tempMax,
                 amplitude=fold_amplitude,
                 width=fold_width):
    """
    A simple approximation for T that increases from depth z=0 with T0 to z=z(bed) with Tbed (-3°C?) is:

    z>C: T(z) = T0

    z<C: T(z)=    T0 + (Tbed - T0) * ( 1. - y / deltay )**2)

    Question is were C is (about 2/3 down towards the bed?).

    For the initial bump you can do: C(x) = C0 - ∆C exp(-4x^2/a^2)
    This is Bercovici's fold, which has an amplitude of ∆C and a width of 2a. Within the fold, the thermal profile is simply stretched, while above the temperature is T0 (say -33°).

    !!! The function as implemented assumes that the bed is at z = 0 !!!

    """
    global particleTemperature
    global swarm

    particleTemperature.data[:] = T0

    # the bump
    xmidpoint, halfwavelength = 0., width / 2.
    xmin = xmidpoint - halfwavelength
    xmax = xmin + width

    for index, coord in enumerate(swarm.data):

        # 'Tbed-core' situation
        ybase = bumpfunc(coord[0], xmin, xmax, amplitude)

        if coord[1] <= ybase + y0:
            particleTemperature.data[index] = T0 + (Tbed - T0) * (
                1. - (coord[1] - ybase) / y0)

        if coord[1] <= ybase:
            particleTemperature.data[index] = Tbed

def setup_density_profile_from_temperature():
    particleDensity.data[:] = densityFn.evaluate(swarm)


# +
def define_caxes_warped_by_folding(saveFile=False):
    '''
    this function defines an indexed 2D array, where single positions store the angles 
    of c-axes at the given location.

    basis for the calculation is the assumption that:

        a) the fold amplitude decreases toward the surface (where it is 0) and is at a max at the base of the model
        b) this permits the calculation of the amplitude at every height
        c) the width of the fold is calculated by its area: because the displaced mass has to be the same at any height within the ice sheet, the fold width has to increase if the amplitude decreases. Fold width is inf if amplitude is 0 at the surface
        d) basis for the fold calculation is a cosine curve, which is approximated by the first to members of the corresponding taylor series (which is a bit easier to calculate

        NOT USING TAYLOR ANYMORE!

        Just cos and diff(cos) proved to be easier and more convenient

    '''

    print("In c-axis setup")

    size, H0, L0 = total_width, fold_amplitude, fold_width

    # slope of the amplitude function (decreases to 0 at surface)
    c = -H0 / (ice_height - H0)
    H0p = H0 - c * H0

    x = np.linspace(int(-size/2), int(size/2), int(2.*size))

    angle_arr = np.ones((x.shape[0], int(maxY)), dtype=np.double) * np.pi / 2.

    print(angle_arr.shape)

    '''
    def y(x, H, yb, L):
        return (1 - 2 * (np.pi * x / L)**2 + 4 * (np.pi * x / L)**4
                ) * 4 * H - 3 * H + yb if x > -L / 2. and x < L / 2. else 0.
    '''

    # I believe there was an error in the following function for very small Ls, 
    # therefore the if loop
    def y(x, H, yb, L):
        if x > -L / 2. and x < L / 2:
            d = (( np.cos ( 2. * x / L * np.pi ) + 1. ) * H/2. ) + yb
            if d > 0:
                return (d)
            else:
                return (0.)
        else:
            return(0.)
    yarr = np.vectorize(y)

    # angle of director = arctan of slope + np.pi/2
    '''
    def y_angle(x, L):
        return np.arctan(
            -16 * H * x / (L**2) + 64 * H * x**3 /
            (L**4)) 
            + np.pi / 2. if x > -L / 2 and x < L / 2 else np.pi / 2.
    '''

    def y_angle(x, L, H):
        return (np.arctan( -np.pi * H * np.sin( -2. * np.pi * -x / L ) / L ) + np.pi/2. if x > -L and x < L else np.pi/2.) 
    y_anglearr = np.vectorize(y_angle)

    for ymid in np.arange(0., ice_height, 0.5):

        H = H0p + c * ymid
        yb = ymid - H

        if H == 0.:
            L = inf
        else:
            L = np.divide(H0 * L0, H)

        angle_arr[x.astype(int) + int(size) - 1,
                  yarr(x, H, yb, L).astype(int)] = y_anglearr(x, L, H)

    angle_arr[:, 2500:] = np.pi/2.

    pos = fn.input().evaluate(swarm)

    director.data[:, 0] = np.cos(angle_arr[pos[:, 0].astype(int) + int(size),
                                           pos[:, 1].astype(int)])
    director.data[:, 1] = np.sin(angle_arr[pos[:, 0].astype(int) + int(size),
                                           pos[:, 1].astype(int)])

    # save data to a file, open it with fct 'load_caxes_from_file'
    if saveFile:
        size = MODEL_DATA['MAX_X'] - MODEL_DATA['MIN_X']
        np.save(f'../c_axes_angles-{size}-{maxY}.npy', angle_arr)

    print("Max / Min angle of the c-axes: " + str(np.max(angle_arr)) + ", " +
          str(np.min(angle_arr)))

    # we already defined a swarm (initialFoldSwarm), which has now to be filled with particles
    initialFoldPoints = np.zeros((int(number_of_deformation_points), 2))
    initialFoldPoints[:, 0] = np.linspace(MODEL_DATA['MIN_X'], MODEL_DATA['MAX_X'],
                                          int(number_of_deformation_points))

    # redefine lambda function in order to return y, not 0, outside the fold
    def y(x, H, yb, L):
        return (1 - 2 * (x / L)**2 + 4 * (x / L)**4
                ) * 4 * H - 3 * H + yb if x > -L / 2. and x < L / 2. else yb

    yarr = np.vectorize(y)

    for y in np.arange(0, maxY, distance_between_deformation_lines):

        H = H0p + c * y
        yb = y - H

        if H == 0.:
            L = inf
        else:
            L = np.divide(H0 * L0, H)

        initialFoldPoints[:, 1] = yarr(initialFoldPoints[:, 0], H, yb, L)
        initialFoldSwarm.add_particles_with_coordinates(initialFoldPoints)

    calc_caxes_angles()

def load_caxes_from_file():

    print("loading c-axis setup")

    amplitude = fold_amplitude
    width = fold_width

    global director
    
    director.data[:] = (0., 1.)

    size, H0, L0, ys = total_width, amplitude, width, ice_height

    c = -H0 / (ys - H0)
    H0p = H0 - c * H0

    pos = fn.input().evaluate(swarm)

    # uncomment this, if you want to import the c-axes from a file

    size = MODEL_DATA['MAX_X'] - MODEL_DATA['MIN_X']
    filename = f'../c_axes_angles-{size}-{MODEL_DATA["MAX_Y"]}.npy'
    angle_arr = np.load(filename)  # from tempfile import TemporaryFile
    
    director.data[:, 0] = np.cos(angle_arr[pos[:, 0].astype(int) + int(size), 
                                            pos[:, 1].astype(int)])
    director.data[:, 1] = np.sin(angle_arr[pos[:, 0].astype(int) + int(size),
                                           pos[:, 1].astype(int)])

    print("Max / Min angle of the c-axes: " + str(np.max(angle_arr)) + ", " +
          str(np.min(angle_arr)))

    # we already defined a swarm (initialFoldSwarm), which has now to be filled with particles
    initialFoldPoints = np.zeros((int(number_of_deformation_points), 2))
    initialFoldPoints[:, 0] = np.linspace(MODEL_DATA['MIN_X'], MODEL_DATA['MAX_X'], int(number_of_deformation_points))

    # redefine lambda function in order to return y, not 0, outside the fold
    def y(x, H, yb, L):
        return (1 - 2 * (x / L)**2 + 4 * (x / L)**4
                ) * 4 * H - 3 * H + yb if x > -L / 2. and x < L / 2. else yb

    yarr = np.vectorize(y)

    for y in np.arange(0, ys, distance_between_deformation_lines):

        H = H0p + c * y
        yb = y - H

        if H == 0.:
            L = inf
        else:
            L = np.divide(H0 * L0, H)

        initialFoldPoints[:, 1] = yarr(initialFoldPoints[:, 0], H, yb, L)
        initialFoldSwarm.add_particles_with_coordinates(initialFoldPoints)

    calc_caxes_angles()


# -

#setup_T_bump_squared()
setup_T_bump_linear()
setup_density_profile_from_temperature()
#define_caxes_warped_by_folding(saveFile=True)
print(os.getcwd())
load_caxes_from_file()
# calc_caxes_angles()

'''
print('C-Axes')
calc_caxes_angles()
figCAxes = vis.Figure(figsize=(1800, 500))
figCAxes.append(
    vis.objects.Points(swarm, cAngle, colours=["blue", "green", "yellow", "orange", "red"], pointSize=3.0))
figCAxes.save("c-axes.png")

print('C-Axes + Fold')
figCAxesFold = vis.Figure(figsize=(1800, 500))
figCAxesFold.append(
    vis.objects.Points(initialFoldSwarm, pointSize=3.0, colourBar=False))
figCAxesFold.append(
    vis.objects.Points(swarm, cAngle, colours=["blue", "green", "yellow", "orange", "red"], pointSize=3.0))
figCAxesFold.save("c-axes+fold.png")

print('Initial fold swarm')
figFold = vis.Figure(figsize=(1800, 500))
figFold.append(
    vis.objects.Points(initialFoldSwarm, pointSize=3.0, colourBar=False))
figFold.save("Initial fold swarm")

print('Deformation')
figDef = vis.Figure(figsize=(1800, 500))
figDef.append(
    vis.objects.Points(deformationSwarm, pointSize=3.0, colourBar=False))
figDef.save("deformation.png")

print('Material')
figMaterial = vis.Figure(figsize=(1800, 500))
figMaterial.append(vis.objects.Points(swarm, materialVariable, pointSize=4.0, fn_mask=materialVariable))
figMaterial.save_image("material.png")

print('Density')
figDensity = vis.Figure(figsize=(1800, 500))
figDensity.append(vis.objects.Surface(
    mesh, 
    densityFn))
figDensity.save_image("density.png")

print('Viscosity')
figViscosity = vis.Figure(figsize=(1800, 500))
figViscosity.append(vis.objects.Surface(
    mesh,
    viscosityFn,
    #coord[1],
    #bgStrainrate_high_T,
))
figViscosity.save_image("viscosity.png")

print('Directors')
figDirector = vis.Figure(figsize=(1800,500))
figDirector.append(vis.objects.VectorArrows(mesh, director, resolution=[30, 10, 1], autoscale=True,))
figDirector.save_image("director.png")

print('Log-Viscosity')
figLogViscosity = vis.Figure(figsize=(1800, 500))
figLogViscosity.append(vis.objects.Surface(
    mesh,
    logViscosityFn,
))
figLogViscosity.save_image("logviscosity.png")

print('Pressure')
figPressure = vis.Figure(figsize=(1800, 500))
figPressure.append(vis.objects.Surface(mesh, pressureField))
figPressure.save_image("pressure.png")

#figCorrectedPressure = vis.Figure(figsize=(1800, 500))
#figCorrectedPressure.append( vis.objects.Surface(mesh, meshCorrectedPressure) )
# figCorrectedPressure.show()

#figMeshShearStressCorrected = vis.Figure(figsize=(1800, 500))
#figMeshShearStressCorrected.append( vis.objects.Surface(mesh, meshShearStress) )
# figMeshShearStressCorrected.show()
# figMeshStress.save_image("meshStress.png")

print('Horizontal shear stress')
figHorShearStress = vis.Figure(figsize=(1800, 500))
figHorShearStress.append(vis.objects.Surface(mesh, devStressFn[2]))
figHorShearStress.save("horizontal_shar_stress.png")

print('Temperature')
figT = vis.Figure(figsize=(1800, 500))
figT.append(vis.objects.Points(swarm, particleTemperature, pointSize=3.0))
figT.save_image("temperature.png")

'''

# +
# # Functions

# # Solver and boundary conditions

iWalls = mesh.specialSets["MinI_VertexSet"] + \
    mesh.specialSets["MaxI_VertexSet"]
jWalls = mesh.specialSets["MinJ_VertexSet"] + \
    mesh.specialSets["MaxJ_VertexSet"]
base = mesh.specialSets["MinJ_VertexSet"]
top = mesh.specialSets["MaxJ_VertexSet"]

velocityField.data[:] = [0., 0.]

botSet = mesh.specialSets['Bottom_VertexSet']
topSet = mesh.specialSets['Top_VertexSet']
leftSet = mesh.specialSets['Left_VertexSet']
rightSet = mesh.specialSets['Right_VertexSet']

# cond_dirichlet = uw.conditions.DirichletCondition(variable=velocityField, indexSetsPerDof=(leftSet + rightSet, topSet))

cond_dirichlet = uw.conditions.DirichletCondition(
    variable=velocityField,
    indexSetsPerDof=(botSet + leftSet + rightSet, botSet + topSet ))

velocityField.data[:] = [0., 0.]

stokes = uw.systems.Stokes(
    velocityField=velocityField,
    pressureField=pressureField,
    voronoi_swarm=swarm,
    conditions=[
        cond_dirichlet,
    ],
    fn_viscosity=viscosityFn,
    _fn_viscosity2=viscosityFn2,
    _fn_director=director,
    fn_bodyforce=buoyancyFn,
)

solver = uw.systems.Solver(stokes)

# solver.set_inner_method("lu")
# solver.set_inner_method("superlu")
# solver.set_inner_method("mumps")
solver.set_inner_method("mg")

# solver.set_penalty(1.0e10)  # higher penalty = larger stability
# solver.options.scr.ksp_rtol = 1.0e-3

surfaceArea = uw.utils.Integral(fn=1.0,
                                mesh=mesh,
                                integrationType='surface',
                                surfaceIndexSet=top)
surfacePressureIntegral = uw.utils.Integral(fn=pressureField,
                                            mesh=mesh,
                                            integrationType='surface',
                                            surfaceIndexSet=top)


def calibrate_pressure():

    global pressureField
    global surfaceArea
    global surfacePressureIntegral

    (area, ) = surfaceArea.evaluate()
    (p0, ) = surfacePressureIntegral.evaluate()
    pressureField.data[:] -= p0 / area

    print(f'Calibration pressure {p0 / area}')


nl_tol = 1.
#nl_tol = 1e-1
# solver.set_penalty(1.0e10)  # higher penalty = larger stability
# solver.options.scr.ksp_rtol = 1.0e-3

# test it out
try:
    solver.solve(nonLinearIterate=True,
                 nonLinearTolerance=nl_tol,
                 callback_post_solve=calibrate_pressure)
    solver.print_stats()
except:
    print("Solver died early..")
    exit(0)

# ## Figures

# +

# The stress is only guaranteed to be accurate when integrated across an element. Fluctuations
# within an element can be significant. Mapping to the mesh can help


def update_advection_diffusion(t):

    global meshV, maxX, maxY, snowfall_rate, nl_tol
    global calibrate_pressure

    tin = t

    while t > 0.:

        #solver.solve(nonLinearIterate=True, nonLinearTolerance=nl_tol, callback_post_solve=calibrate_pressure)
        solver.solve(nonLinearIterate=True,
                     callback_post_solve=calibrate_pressure)

        # Retrieve the maximum possible timestep for the advection system.
        dt = min([
            advector1.get_max_dt(),
            advector2.get_max_dt(),
            advector3.get_max_dt(),
            advDiff.get_max_dt()
        ])

        if dt > t:
            dt = t
            t = 0.
        else:
            t = t - dt

        advDiff.integrate(dt)
        advector1.integrate(dt)
        # advector2.integrate(dt)
        advector3.integrate(dt)  # the deformation swarm

        # particle population control
        pop_control1.repopulate()
        # pop_control2.repopulate()
        # pop_control3.repopulate()

        particleTemperature.data[:] = temperatureField.evaluate(swarm)
        particleDensity.data[:] = densityFn.evaluate(swarm)

        c_axis_rotation(dt)

    return time + tin


def update_infinite_flow(t):
    """
    In order for infinite flow to work you need to
    a) use the same medication as MC Escher,
    b)	1) generate 'inclination' --> z_hat definition
        2) activate wrapping by setting 'periodic = [True, False]' during the mesh creation
    """

    global meshV, maxX, maxY, snowfall_rate, nl_tol
    global calibrate_pressure

    t *= 364. * 86400.

    tin = t

    while t:

        solver.solve(nonLinearIterate=True, nonLinearTolerance=nl_tol, callback_post_solve=calibrate_pressure)

        # Retrieve the maximum possible timestep for the advection system.
        dt = min([
            advector1.get_max_dt(),
            advector3.get_max_dt(),
            advector4.get_max_dt()
        ])

        print(f'---------------------{dt}')
        # exit(0)

        if dt > t:
            dt = t
            t = 0.
        else:
            t = t - dt

        #print(t)

        # Advect using this timestep size.
        advector1.integrate(dt)  # the swarm
        # advector2.integrate(dt)# the surface swarm
        advector3.integrate(dt)  # the deformation swarm
        advector4.integrate(dt)  # the initialFoldSwarm

        # particle population control
        # not sure, if this is really necessary. it is _not_ in the standard advecton code, but only if there is a mesh-deformation involved. my hope would be that this command will not allow underresolved cells
        pop_control1.repopulate()
        # pop_control2.repopulate()
        # pop_control3.repopulate()

        c_axis_rotation(dt)

    return (tin)

# +
# # Main function


def main():

    # generate output file

    outputFile = os.path.join(os.path.abspath("."), "output.csv")
    if os.path.exists(outputFile):
        os.rename(outputFile, outputFile + ".bkp")

    # Stepping. Initialise time and timestep.

    time = 0.
    step = 0
    nsteps = 100
    delta_timestep = 1.
    output_step = 5
    
    xdmf_mesh = mesh.save('mesh.h5')

    while step < nsteps:

        update_infinite_flow(t = delta_timestep)
        calc_caxes_angles()
        step += 1

        time = step * delta_timestep

        if not step % output_step:


                    ignore = swarm.save('swarm_' + str(step).zfill(5) + '.h5')

        # eval swarm variables
        particleStrainrate.data[:] = strainRate_2ndInvariantFn.evaluate(swarm)
        particleViscosity.data[:] = viscosityFn.evaluate(swarm)
        #particleShearstress.data[:] = shearStressFn.evaluate(swarm)

        # save swarm variables as xdmf files
        xdmf_swarm = swarm.save('swarm_' + str(step).zfill(5) + '.h5')

        xdmf_particleStrainrate = particleStrainrate.save('particleStrainrate_' + str(step).zfill(5) + '.h5')
        particleStrainrate.xdmf('particleStrainrate_' + str(step).zfill(5) + '.xdmf', xdmf_particleStrainrate, 
                                "particleStrainrate", xdmf_swarm, "Swarm", modeltime=step)

        xdmf_Director = director.save('director_' + str(step).zfill(5) + '.h5')
        director.xdmf('director_' + str(step).zfill(5) + '.xdmf', xdmf_Director, "Director", xdmf_swarm, "Swarm", modeltime=step)

        xdmf_particleViscosity = particleViscosity.save('particleViscosity_' + str(step).zfill(5) + '.h5')
        particleViscosity.xdmf('particleViscosity_' + str(step).zfill(5) + '.xdmf', xdmf_particleViscosity, 'particleViscosity', xdmf_swarm, 'Swarm', modeltime=step)

        xdmf_particleCAngle = cAngle.save('cAngle_' + str(step).zfill(5) + '.h5')
        cAngle.xdmf('cAngle_' + str(step).zfill(5) + '.xdmf', xdmf_particleViscosity, 'cAngle', xdmf_swarm, 'Swarm', modeltime=step)

        # visualizing the velocityField in paraviewe doesn't work for whatever reason (Paraview just crashes)
        # so we save it as a particle property
        particleVelocity.data[:]  = velocityField.evaluate(swarm)
        xdmf_particleVelocity = particleVelocity.save('particleVelocity_' + str(step).zfill(5) + '.h5')
        particleVelocity.xdmf('particleVelocity_' + str(step).zfill(5) + '.xdmf', xdmf_particleVelocity, "particleVelocity", xdmf_swarm, "Swarm", modeltime=step)

        print ()
        print ('--------------------')
        print (str(time) + ' years, step: ' + str(step))
        print ('--------------------')
        print ()
        
        '''
        figCAxes.save_image(outputPath + "cAxes-" + str(step).zfill(4) +
                            " - " + str(time) + ".png")
        figCAxesFold.save_image(outputPath + "cAxes-Fold-" + str(step).zfill(4) +
                                " - " + str(time) + ".png")
        figFold.save_image(outputPath + "Fold-" + str(step).zfill(4) +
                                " - " + str(time) + ".png")
        figMaterial.save_image(outputPath + "Material-" + str(step).zfill(4) +
                               " - " + str(time) + ".png")
        figDensity.save_image(outputPath + "Density-" + str(step).zfill(4) +
                                " - " + str(time) + ".png")
        figViscosity.save_image(outputPath + "Viscosity-" +
                                str(step).zfill(4) + " - " + str(time) + ".png")
        figPressure.save_image(outputPath + "Pressure-" + str(step).zfill(4) +
                               " - " + str(time) + ".png")
        figHorShearStress.save_image(outputPath + "HorShearStress-" +
                                str(step).zfill(4) + " - " + str(time) + ".png")
        figT.save_image(outputPath + "T-" + str(step).zfill(4) + " - " +
                        str(time) + ".png")
        figDef.save_image(outputPath + "Deformation-" + str(step).zfill(4) +
                        " - " + str(time) + ".png")
        figLogViscosity.save_image(outputPath + "Logviscosity-" + str(step).zfill(4) +
                        " - " + str(time) + ".png")
        '''
        
        print(f'Finished timestep: {step}')


if __name__ == "__main__":
    main()
# -


