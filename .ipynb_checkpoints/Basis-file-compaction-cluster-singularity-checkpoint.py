#!/usr/bin/env python
# coding: utf-8
# %%
# import modules, create output directory

import os
import matplotlib.pyplot as plt
import numpy as np
import underworld as uw
from underworld import function as fn
import underworld.visualisation as vis
from scipy.spatial import Delaunay, cKDTree
import pickle

uw.utils.matplotlib_inline()

if not os.getcwd().rsplit("/")[-1] == "output":
    outputPath = os.path.join(os.path.abspath("."),"output/")
    if not os.path.exists ( outputPath ):
        os.makedirs ( outputPath )

    os.chdir(outputPath)

# ### Example fold function + plot

# %%
## basic parameters

g = 9.81
#ice_density = 910.

#A = 1e-16
n = 4.

Amplitude = 400.
Width = 20000.

MODEL_DATA = {}

MODEL_DATA['MIN_Y'] = 0.0
MODEL_DATA['MAX_Y'] = 2500.0

MODEL_DATA['MIN_X'] = 0.0
MODEL_DATA['MAX_X'] = 50000.0

iceHeight = 2500.

MODEL_DATA['RES_X'] = 400
MODEL_DATA['RES_Y'] = 20

MODEL_DATA['ELEMENT_TYPE'] = "Q1/dQ0"

MODEL_DATA['PERIODIC_X'] = True
MODEL_DATA['PERIODIC_Y'] = False

MODEL_DATA['PARTICLES_PER_CELL'] = 200

# save basic model data to file
# save a dictionary into pickle file
MODEL_DATA_FILE = outputPath + "model_data.p"
pickle.dump( MODEL_DATA, open(MODEL_DATA_FILE , "wb" ) )

mesh = uw.mesh.FeMesh_Cartesian( elementType = ( MODEL_DATA['ELEMENT_TYPE'] ) ,
                                 elementRes  = ( MODEL_DATA['RES_X'], MODEL_DATA['RES_Y'] ),
                                 minCoord    = ( MODEL_DATA['MIN_X'], MODEL_DATA['MIN_Y'] ),
                                 maxCoord    = ( MODEL_DATA['MAX_X'], MODEL_DATA['MAX_Y'] ),
                                 periodic    = ( MODEL_DATA['PERIODIC_X'], MODEL_DATA['PERIODIC_Y'] )
                               )

velocityField    = mesh.add_variable(         dataType="double",  nodeDofCount=2 )
pressureField    = mesh.subMesh.add_variable( dataType="double",  nodeDofCount=1 )

velocityField.data[:] = [0.,0.]
pressureField.data[:] = 0.

## visualisation parameters

viz_opts = {
                "figsize"     : (2000,400),
                "edgecolour"   :  "black",
                "quality"      :  3,          # antialiasing
                "align"        : "bottom",     # colour bar alignment
                #"size"        : (0.83,0.01), # colour bar size
                #"position"    : 0.,          # colour bar position
                #"boundingBox"  : ((MIN_X, MAX_X), (MIN_Y, MAX_Y), (minZ, maxZ)),
                "axis"         : True,
                "scale"        : True,
           }

# %%
light_layer = 500.

#''' deform the mesh - but we don't do this right now

FOLD_XMIN = 20000.
FOLD_XMAX = 30000.

delta_x = FOLD_XMAX - FOLD_XMIN
omega = 2. * np.pi / delta_x

amplitude = 250. # do not mix up with half-amplitude!!!

coord = fn.input()

z_top_function_def = amplitude/2. + amplitude/2. * (fn.math.sin(omega * (coord[0] - FOLD_XMIN - delta_x / 4.) ) )

z_top_function = [ 
               (       coord[0] < FOLD_XMIN,   MODEL_DATA['MIN_Y']     ),
               (       coord[0] > FOLD_XMAX,   MODEL_DATA['MIN_Y']     ),
               (       True ,  z_top_function_def ), 
             ]


# %%
x = np.linspace(10000., 40000., 101).reshape(-1,1)
y = np.linspace(0., 0., 101).reshape(-1,1)

xy = np.concatenate((x,y), axis=1)

new_y = fn.branching.conditional( z_top_function ).evaluate(xy)

new_xy = np.concatenate((x, new_y), axis=1)

plt.plot(new_xy[:,0], new_xy[:,1])


# %%
dx = (MODEL_DATA['MAX_X'] - MODEL_DATA['MIN_X']) / MODEL_DATA['RES_X']
dy = (MODEL_DATA['MAX_X'] - MODEL_DATA['MAX_Y']) / MODEL_DATA['RES_Y']

def mesh_deform_Ind(section, fix_point):
    
    global MESH_DATA
    
    section[0] = fix_point
    seq = len(section)
    delta_y = (MODEL_DATA['MAX_Y'] - fix_point) / float(seq)
        
    # fixPoint_index (int): specify the index of the section to be at the place need to be refined
    # fixPoint: the position to be refined
    # mi: representing the gradient of mesh resolution; the larger mi, the larger gradient
    
    for index in range(1, seq):
        section[index] = index * delta_y + fix_point          

    return (section)

with mesh.deform_mesh():
    
    for indexx in range(MODEL_DATA['RES_X'] + 1):
        
        start_x = dx * indexx
        
        interface_y = fn.branching.conditional( z_top_function ).evaluate((start_x, 0.))[0][0]         
        
        ind = np.where( abs(mesh.data[:, 0] - start_x) < 0.01*dx )
        
        mesh.data[ind[0],1] = mesh_deform_Ind(mesh.data[ind[0], 1], interface_y)


# %%
figMesh = vis.Figure(figsize=(1200,600))
figMesh.append( vis.objects.Mesh(mesh))
figMesh.show()


# %%
# Create a swarm which will define our material geometries, and will also
# track deformation and history dependence of particles.
swarm  = uw.swarm.Swarm( mesh=mesh, particleEscape=True)

swarmLayout = uw.swarm.layouts.PerCellSpaceFillerLayout( swarm=swarm, particlesPerCell=MODEL_DATA['PARTICLES_PER_CELL'] )
swarm.populate_using_layout( layout=swarmLayout )

# create pop control object
pop_control1 = uw.swarm.PopulationControl(swarm, aggressive=True, aggressiveThreshold=0.7, particlesPerCell=MODEL_DATA['PARTICLES_PER_CELL'])

# create advector
advector1 = uw.systems.SwarmAdvector(swarm=swarm,velocityField=velocityField, order=2)


# %%
# Initialise particle properties 
materialVariable = swarm.add_variable( dataType="int", count=1 )
particleDensity = swarm.add_variable ( dataType="double", count=1 )

particleStrainrate = swarm.add_variable ( dataType="double", count=1 )
particleViscosity = swarm.add_variable ( dataType="double", count=1 )
particleShearstress = swarm.add_variable ( dataType="double", count=1 )

particleDirector = swarm.add_variable ( dataType="double", count=2 )

particleTemperature = swarm.add_variable ( dataType="double", count=1 ) ###
particleTemperature.data[:] = 0.

particleVelocity = swarm.add_variable( dataType="double", count=2 )
particleVelocity.data[:] = (0.,0.)

# %%
# Definition of temperature field and density

materialV = 0  # viscoplastic ice

materialVariable.data[:] = materialV

coord = fn.input()

T_0 = -30.
T_BED = -3.

# T_function = 

conditions = [ 
               (       coord[0] > light_layer + z_top_function_def,    T_0    ),
               (       True                  ,    T_BED  ),
             ]

particleTemperature.data[:] = fn.branching.conditional( conditions ).evaluate(swarm)

particleDirector.data[:] = (0., 1.)

figT = vis.Figure(**viz_opts, title="Temperature")
#figT.append(vis.objects.Points(swarm, materialVariable, pointSize=1.0, fn_mask=materialVariable, colourBar=False))
figT.append(vis.objects.Points(swarm, particleTemperature, pointSize=1.0, colourBar=True))
figT.show()


# %%
# functions, incl flow law
R = 0.008314 # kJ / (T*mol)
QhighT = 181. # kJ/mol, activation energy
QsmallT = 60.

strainRateTensor = fn.tensor.symmetric(velocityField.fn_gradient)
strainRate_2ndInvariantFn = fn.tensor.second_invariant(strainRateTensor)

minViscosityIceFn  = fn.misc.constant(1e+8 / 3.1536e7)
maxViscosityIceFn  = fn.misc.constant(1e+14 / 3.1536e7)

# Goldsby 2006, in 1 / (MPa**4 * s)
AlowT = 1.2e6
QlowT = 60.0

AhighT = 6.0e28
QhighT = 181.0

# scaling to Pa a
AlowT *= 1e6 / (86400. * 365.)
AhighT *= 1e6 / (86400. * 365.)

V1 = 0.5 * (AlowT * fn.math.exp(-QlowT / (R*(particleTemperature + 273.)))) ** (-1./n) * (strainRate_2ndInvariantFn**((1.-n) / n))
V2 = 0.5 * (AhighT * fn.math.exp(-QhighT / (R*(particleTemperature + 273.)))) ** (-1./n) * (strainRate_2ndInvariantFn**((1.-n) / n))

VisBaseconditions = [
                (       particleTemperature <= -15., V1 ), 
                (       True,                        V2),               
                ]

viscosityFn = fn.branching.conditional( VisBaseconditions )

viscosityFn = fn.misc.max(fn.misc.min(viscosityFn, maxViscosityIceFn), minViscosityIceFn)

particleViscosity.data[:] = viscosityFn.evaluate(swarm)

densityFnIce = (18.02 / (19.30447 - 7.988471e-4 * (particleTemperature+273.) + 7.563261e-6 * ((particleTemperature+273.)**2) )) * 1000.
densityFnAir = fn.misc.constant( 0. )

particleDensity.data[:] = densityFnIce.evaluate(swarm)

z_hat = (0.00872653549837393496, -0.99996192306417128874)
buoyancyFn = densityFnIce * z_hat * 9.81


# %%
figVisc = vis.Figure(**viz_opts, title="Viscosity")
visc = vis.objects.Points(swarm, particleViscosity, pointSize=1.0, colourBar=True)
figVisc.append(visc)
figVisc.show()

figS = vis.Figure(**viz_opts, title="Strainrate")
figS.append(vis.objects.Points(swarm, strainRate_2ndInvariantFn, pointSize=3.0, colourBar=True))
figS.show()

figDensity = vis.Figure(**viz_opts, title="Density")
figDensity.append(vis.objects.Points(swarm, particleDensity, pointSize=1.0, colourBar=True))
figDensity.show()


# %%
## set boundary conditions
iWalls = mesh.specialSets["MinI_VertexSet"] + mesh.specialSets["MaxI_VertexSet"]
jWalls = mesh.specialSets["MinJ_VertexSet"] + mesh.specialSets["MaxJ_VertexSet"]

base   = mesh.specialSets["MinJ_VertexSet"]
top    = mesh.specialSets["MaxJ_VertexSet"]
leftWall = mesh.specialSets["MinI_VertexSet"]
rightWall = mesh.specialSets["MaxI_VertexSet"]

allWalls = iWalls + jWalls

velocityField.data[:] = (0., 0.)
velocityField.data[leftWall] = (1., 0.)
velocityField.data[rightWall] = (-1., 0.)

velocityBCs = uw.conditions.DirichletCondition(
                                                variable        = velocityField, 
                                                indexSetsPerDof = (base + leftWall + rightWall, base),
                                              )

# %%

## setup solver and solve

stokes = uw.systems.Stokes(
    velocityField=velocityField,  
    pressureField=pressureField,
    voronoi_swarm=swarm,
    conditions=velocityBCs,
    fn_viscosity=viscosityFn,
    fn_bodyforce=buoyancyFn,

    #_fn_viscosity2=viscosityFn2,
    #_fn_director=directorField,
    #_fn_director=particleDirector,
    
    )

solver = uw.systems.Solver(stokes)

# solver.set_inner_method("mumps")
# solver.options.scr.ksp_type="cg"
#solver.set_penalty(1.0e7) # higher penalty = larger stability + (often) faster calculation
# solver.options.scr.ksp_rtol = 1.0e-3

surfaceArea = uw.utils.Integral( fn=1.0, mesh=mesh, integrationType='surface', surfaceIndexSet=top)
surfacePressureIntegral = uw.utils.Integral( fn=pressureField, mesh=mesh, integrationType='surface', surfaceIndexSet=top)

def calibrate_pressure():

    global pressureField
    global surfaceArea
    global surfacePressureIntegral

    (area,) = surfaceArea.evaluate()
    (p0,) = surfacePressureIntegral.evaluate() 
    pressureField.data[:] -= p0 / area

    # print (f'Calibration pressure {p0 / area}')

# test it out
try:
    #solver.solve(nonLinearIterate=True, nonLinearTolerance=nl_tol, callback_post_solve=calibrate_pressure)
    solver.solve(nonLinearIterate=True, callback_post_solve=calibrate_pressure)
    solver.print_stats()
except:
    print("Solver died early..")
    exit(0)


# %%
figVel = vis.Figure(**viz_opts, title="Velocity")
figVel.append(vis.objects.VectorArrows(mesh, velocityField, ))
#figVel.append(vis.objects.Points(surfaceSwarm, pointSize=1.0, colourBar=False, colour='Blue'))
#figVel.append(vis.objects.Surface(mesh, uw.function.math.dot(velocityField,velocityField), colours="gebco"))

figVel.show()


# %%


figVisc = vis.Figure(**viz_opts, title="Viscosity")
visc = vis.objects.Points(swarm, particleViscosity, pointSize=2.0, colourBar=True)
figVisc.append(visc)
figVisc.show()

figP = vis.Figure(**viz_opts, title="Pressure")
figP.append(vis.objects.Surface(mesh, pressureField, ))
figP.show()


# %%


def c_axis_rotation(dt, steps = 1.):

    dt /= steps
    
    for i in range(0, int(steps)):
        
        iceIndices = np.array(np.where(materialVariable.data == materialV)[0])
        
        velGrad = velocityField.fn_gradient.evaluate(  swarm ).reshape(swarm.particleGlobalCount , mesh.dim, mesh.dim)
        velGrad = velGrad[iceIndices]
        velGradT = velGrad.swapaxes(-1,1)

        # rate of deformation and rate of rotation
        D = 0.5 * (velGrad + velGradT)
        W = 0.5 * (velGrad - velGradT)

        particleDirector.data[iceIndices] = particleDirector.data[iceIndices] + dt * ( np.einsum("ijk,ik->ij", W, particleDirector.data[iceIndices]) - np.einsum("ijk,ik->ij", D, particleDirector.data[iceIndices]) + np.einsum("ij,ij->i",particleDirector.data[iceIndices], np.einsum("ijk,ik->ij",D,particleDirector.data[iceIndices]))[:,None] * particleDirector.data[iceIndices])

        #finally normalize the c-axes
        particleDirector.data[iceIndices] = particleDirector.data[iceIndices] / np.absolute(np.linalg.norm(particleDirector.data[iceIndices], axis=1).reshape(len(iceIndices),1))

        # we want to rotate all directors, if they point towards the negative y-direction
        # this should make it easier to display them
        b = np.where(particleDirector.data[:,1] < 0.)
        particleDirector.data[b] *= -1.


# %%


def flow(rotate_caxes = False):
    
    global calibrate_pressure
    global advector1
    global pop_control1
       
    #solver.solve(nonLinearIterate=True, nonLinearTolerance=nl_tol, callback_post_solve=calibrate_pressure)
    solver.solve(nonLinearIterate=True, callback_post_solve=calibrate_pressure)

    # Retrieve the maximum possible timestep for the advection system.
    dt = advector1.get_max_dt()

    #print(f'Calculating {dt}, leaving {t}')

    # Advect using this timestep size.
    advector1.integrate(dt) # the swarm
    #advector2.integrate(dt) # the surface swarm

    # particle population control
    pop_control1.repopulate()
    #pop_control2.repopulate()

    if rotate_caxes:
        c_axis_rotation(dt, steps = 100.)
        #c_axis_rotation_mesh(dt, steps = 100.)

    return (dt)


# %%


maxSteps = 10
stepsize = 1.   

step = 0
t = 0.

xdmf_mesh    = mesh.save('mesh.h5')

while step < maxSteps:

    print ("in step " + str(step))

    t += 1#flow(rotate_caxes = False)
    
    if not step%stepsize:
        
        ignore = swarm.save('swarm_' + str(step).zfill(5) + '.h5')
        
        # eval swarm variables
        particleStrainrate.data[:] = strainRate_2ndInvariantFn.evaluate(swarm)
        particleViscosity.data[:] = viscosityFn.evaluate(swarm)
        #particleShearstress.data[:] = shearStressFn.evaluate(swarm)
        
        # save swarm variables as xdmf files
        xdmf_swarm = swarm.save('swarm_' + str(step).zfill(5) + '.h5')
        
        xdmf_particleStrainrate = particleStrainrate.save('particleStrainrate_' + str(step.zfill(5)) + '.h5')
        particleStrainrate.xdmf('particleStrainrate_' + str(step).zfill(5) + '.xdmf', xdmf_particleStrainrate, 
                                "particleStrainrate", xdmf_swarm, "Swarm", modeltime=step)

        #xdmf_particleDirector = particleDirector.save('particleDirector_' + str(step.zfill(5)) + '.h5')
        #particleDirector.xdmf('particleDirector_' + str(step.zfill(5)) + '.xdmf', xdmf_particleDirector, 
        #                      "particleDirector", xdmf_swarm, "Swarm", modeltime=step)

        #xdmf_particleMeshDirector = particleMeshDirector.save('particleMeshDirector_' + str(step) + '.h5')
        #particleMeshDirector.xdmf('particleMeshDirector_' + str(step) + '.xdmf', xdmf_particleMeshDirector, 
        #                      "particleMeshDirector", xdmf_swarm, "Swarm", modeltime=step)
        
        xdmf_particleViscosity = particleViscosity.save('particleViscosity_' + str(step).zfill(5) + '.h5')
        particleViscosity.xdmf('particleViscosity_' + str(step).zfill(5) + '.xdmf', xdmf_particleViscosity, 
                                "particleViscosity", xdmf_swarm, "Swarm", modeltime=step)
        
        figDensity.save(f'density_step_{step}_time_{t}_a.png')
        figVel.save(f'velocity_step_{step}_time_{t}_a.png')
        figP.save(f'pressure_step_{step}_time_{t}_a.png')
    
        
        # visualizing the velocityField in paraviewe doesn't work for whatever reason (Paraview just crashes)
        # so we save it as a particle property
        particleVelocity.data[:]  = velocityField.evaluate(swarm)
        xdmf_particleVelocity = particleVelocity.save('particleVelocity_' + str(step.zfill(5)) + '.h5')
        particleVelocity.xdmf('particleVelocity_' + str(step).zfill(5) + '.xdmf', xdmf_particleVelocity, 
                      "particleVelocity", xdmf_swarm, "Swarm", modeltime=step)
        
        print (str(t) + ' years, step: ' + str(step))
        
    step += 1



# %%
